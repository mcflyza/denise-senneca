module.exports = {
  siteMetadata: {
    title: `Denise Senneca Official Website`,
    description: `A personal web space for Denise Senneca PH`,
    author: `@mcflyza`,
    siteUrl: `https://denisesenneca.com`
  },
  plugins: [
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: "G-K56KXZLZQF",
      },
    },
    `gatsby-plugin-react-helmet`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/../src/static`,
      },
    },
    `gatsby-transformer-sharp`,
    `gatsby-plugin-sharp`,
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `gatsby-starter-default`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`
      }
    },
    {
      resolve: `gatsby-plugin-sass`,
      options: {
        implementation: require("sass"),
        useResolveUrlLoader: {
          options: {
            postCssPlugins: [require(`postcss-preset-env`)({ stage: 0 })],
            debug: true,
            sourceMap: true
          }
        }
      }
    },
    {
      resolve: `gatsby-plugin-minify-classnames`,
      options: {
        develop: true,
      }
    },
    `gatsby-plugin-sitemap`
  ]
}
