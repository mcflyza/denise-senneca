import React from 'react'

import Header from '../components/shared/header/header'
import Footer from '../components/shared/footer/footer'
import FineArtsComponent from '../components/shared/photography/fineArtsComponent'
import ReportageComponent from '../components/shared/photography/reportageComponent'
import StreetComponent from '../components/shared/photography/StreetComponent'
import AdvertisingComponent from '../components/shared/photography/advertisingComponent'
import HelmetComponent from '../components/shared/helmetComponenet'

import '../styles/index.scss'
import '../styles/pages/photography/photography.scss';

const Photography = () => (
	<>
		<HelmetComponent />
		<Header />
		<FineArtsComponent />
		<ReportageComponent />
		<StreetComponent />
		<AdvertisingComponent />
		<Footer />
	</>
)

export default Photography;
