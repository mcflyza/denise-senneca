import React from 'react'

import Header from '../../components/shared/header/header'
import Footer from '../../components/shared/footer/footer'
import ReportageComponent from '../../components/shared/photography/reportageComponent'
import HelmetComponent from '../../components/shared/helmetComponenet'

const Reportage = () => (
	<>
		<HelmetComponent />
		<Header />
		<ReportageComponent />
		<Footer />
	</>
)

export default Reportage;
