import React from 'react'

import Header from '../../components/shared/header/header'
import Footer from '../../components/shared/footer/footer'
import StreetComponent from '../../components/shared/photography/StreetComponent'
import HelmetComponent from '../../components/shared/helmetComponenet'

const Street = () => (
	<>
		<HelmetComponent />
		<Header />
		<StreetComponent />
		<Footer />
	</>
)

export default Street;
