import React from 'react'

import Header from '../../components/shared/header/header'
import Footer from '../../components/shared/footer/footer'
import AdvertisingComponent from '../../components/shared/photography/advertisingComponent'
import HelmetComponent from '../../components/shared/helmetComponenet'

const Advertising = () => (
	<>
		<HelmetComponent />
		<Header />
		<AdvertisingComponent />
		<Footer />
	</>
)

export default Advertising;
