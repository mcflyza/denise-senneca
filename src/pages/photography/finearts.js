import React from 'react'

import Header from '../../components/shared/header/header'
import Footer from '../../components/shared/footer/footer'
import FineArtsComponent from '../../components/shared/photography/fineArtsComponent'
import HelmetComponent from '../../components/shared/helmetComponenet'

const FineArts = () => (
	<>
		<HelmetComponent />
		<Header />
		<FineArtsComponent />
		<Footer />
	</>
)

export default FineArts;
