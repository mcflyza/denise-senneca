import React from "react"

import Header from "../../components/shared/header/header"
import Footer from "../../components/shared/footer/footer"
import BloodComponent from "../../components/shared/performances/blood/bloodComponent"
import HelmetComponent from '../../components/shared/helmetComponenet'

const Blood = () => (
	<>
		<HelmetComponent />
		<Header />
		<BloodComponent />
		<Footer />
	</>
)

export default Blood
