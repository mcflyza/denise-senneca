import React from "react"

import Header from "../../components/shared/header/header"
import Footer from "../../components/shared/footer/footer"
import PleaseTellMeAndCryComponent from "../../components/shared/performances/pleaseTellMeAndCry/pleaseTellMeAndCryComponent"
import HelmetComponent from '../../components/shared/helmetComponenet'

const Blood = () => (
    <>
        <HelmetComponent />
        <Header />
        <PleaseTellMeAndCryComponent />
        <Footer />
    </>
)

export default Blood
