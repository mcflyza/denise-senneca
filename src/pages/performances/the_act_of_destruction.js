import React from "react"

import Header from "../../components/shared/header/header"
import Footer from "../../components/shared/footer/footer"
import TheActOfDestructionComponent from "../../components/shared/performances/theActOfDestruction/theActOfDestructionComponent"
import HelmetComponent from '../../components/shared/helmetComponenet'

const Blood = () => (
    <>
        <HelmetComponent />
        <Header />
        <TheActOfDestructionComponent />
        <Footer />
    </>
)

export default Blood
