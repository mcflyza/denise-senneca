import React from 'react'

import Header from '../components/shared/header/header'
import ResponsiveImage from '../components/shared/responsiveImage/responsiveImage'
import Footer from '../components/shared/footer/footer'
import HelmetComponent from '../components/shared/helmetComponenet'

import '../styles/index.scss'
import '../styles/pages/biography/biography.scss';

const Biography = () => {

  let srcSetImgs = {
	smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1568229263/Biography/DeniseSenecaBiographyPhoto_r4xafn.jpg',
	tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1568229263/Biography/DeniseSenecaBiographyPhoto_r4xafn.jpg',
	tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1568229263/Biography/DeniseSenecaBiographyPhoto_r4xafn.jpg',
	desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1568229263/Biography/DeniseSenecaBiographyPhoto_r4xafn.jpg',
	deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1568229263/Biography/DeniseSenecaBiographyPhoto_r4xafn.jpg',
	smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1568229263/Biography/DeniseSenecaBiographyPhoto_r4xafn.jpg',
	tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1568229263/Biography/DeniseSenecaBiographyPhoto_r4xafn.jpg',
	tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1568229263/Biography/DeniseSenecaBiographyPhoto_r4xafn.jpg',
	deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1568229263/Biography/DeniseSenecaBiographyPhoto_r4xafn.jpg',
	deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1568229263/Biography/DeniseSenecaBiographyPhoto_r4xafn.jpg',
	altValue: 'denise senneca'
  }
  

  return (
	<>
	  <HelmetComponent />
	  <Header />
	  <section className = 'biography__wrapper'>
		<h3 className = 'biography__wrapper__title'>Biography</h3>
		<div className = 'imgBiographyWrapper'>
			<ResponsiveImage className='biography__wrapper__mainImg' srcSetImgs = {srcSetImgs}/>
		</div>
		<div className = 'biography'>
		  <section className = 'biography__section education'>
			<h3>Education</h3>
			{/* <div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Mar 2019</span>
			  <p className = 'biography__section__content__what_title'>The Dots and Karmarama - Portfolio Masterclass Advertising Creatives A Step-By-Step guide for Designers, Advertisers and Creatives. Learn the behind-the-scenes skills needed for creative careers</p>
			  <span className = 'biography__section__content__where'>London</span>
			</div> */}
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Feb 2018</span>
			  <p className = 'biography__section__content__what_title'><strong>RUFA - Rome University of Fine Arts</strong></p>
			  <p className = 'biography__section__content__what_description'><em>BA Degree Program in Photography, GPA: 110 cum laude</em><br/>Core subjects: Phenomenology of Contemporary Arts 30L, Advertising Communication 30L, Mass Media Theory & Methods 30L, Digital Video & Editing 30L, Digital Image Processing 30L</p>
			  <span className = 'biography__section__content__where'>Rome</span>
			</div>
			{/* <div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Mar 2017</span>
			  <p className = 'biography__section__content__what_title'>Multidisciplinary laboratory focused on documentation and self-cultivation: ‘Transformers. Can I convey with my body-language the thought of 36000 years of art’ A. Abbatangelo</p>
			  <span className = 'biography__section__content__where'>Rome</span>
			</div> */}
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Jul 2015</span>
			  <p className = 'biography__section__content__what_title'><strong>Classic High School Dante Alighieri</strong></p>
			  <p className = 'biography__section__content__what_description'><em>Diploma Program in Art and Cultural Heritage</em></p>
			  <span className = 'biography__section__content__where'>Rome</span>
			</div>
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Mar 2013</span>
			  <p className = 'biography__section__content__what_title'><strong>Practical theoretical training in photography at Factory 10 Cultural Association</strong></p>
			  <p className = 'biography__section__content__what_description'><em>Photography, photoshop, history of photography</em></p>
			  <span className = 'biography__section__content__where'>Rome</span>
			</div>
		  </section>
		  <section className = 'biography__section experience'>
			<h3>Work Experience</h3>
			<div className='biography__section__content'>
				<span className='biography__section__content__when'>Jun 2019</span>
					<p className='biography__section__content__what_title'><strong>Work experience at Fujifilm Company</strong><br/><em>Photography Promoter and Content Creator</em></p>
				<span className='biography__section__content__where'>London</span>
			</div>			
			<div className='biography__section__content'>
				<span className='biography__section__content__when'>Jun 2019</span>
					<p className='biography__section__content__what_title'><strong>Lead Photographer at TI&SHRT LTD - E-Commerce</strong><br /><em>Photographer and Photo Editor</em></p>
				<span className='biography__section__content__where'>London</span>
			</div>			
			<div className='biography__section__content'>
				<span className='biography__section__content__when'>Nov 2018</span>
					<p className='biography__section__content__what_title'><strong>Work experience at Zari Gallery | Contemporary Fine Art</strong><br /><em>Photographer, Photo Editor, Videographer, Video Editor</em></p>
				<span className='biography__section__content__where'>London</span>
			</div>
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Sep 2017</span>
					<p className='biography__section__content__what_title'><strong>Practical Training at Eleutheria Foundation</strong><br /><em>Photographer and Photo Editor</em></p>
			  <span className = 'biography__section__content__where'>Prague</span>
			</div>
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Sep 2016</span>
					<p className='biography__section__content__what_title'><strong>Collaboration grant at RUFA - Rome University of Fine Arts</strong><br /><em>Photographer and Photo Editor - one academic year -</em></p>
			  <span className = 'biography__section__content__where'>Rome</span>
			</div>
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Sep 2016</span>
					<p className='biography__section__content__what_title'><strong>Collaboration with the Inside Art magazine</strong><br /><em>They supervised and directed my photographic documentation project for a year</em></p>
			  <span className = 'biography__section__content__where'>Rome</span>
			</div>
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Mar 2016</span>
					<p className='biography__section__content__what_title'><strong>Collaboration with GNAM. The National Gallery of Modern and Contemporary Art of Rome</strong><br /><em>Photographer and Photo Editor - exhibitions and press conferences -</em></p>
			  <span className = 'biography__section__content__where'>Rome</span>
			</div>
		  </section>
		  <section className = 'biography__section exhibitions'>
			<h3>Exhibitions</h3>
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Nov 2019</span>
			  <p className = 'biography__section__content__what_title'><strong>#ARTISWHYIWAKEUPINTHEMORNING - The Line London</strong></p>
			  <p className = 'biography__section__content__what_title'>• I exhibited one performance and four different photos from my personal portfolio</p>
			  <span className = 'biography__section__content__where'>London</span>
			</div>
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Aug 2019</span>
			  <p className = 'biography__section__content__what_title'><strong>From Darkness Comes Light – The Freelance Club</strong></p>
			  <p className = 'biography__section__content__what_title'>• I exhibited my artwork ‘The Eye’</p>
			  <span className = 'biography__section__content__where'>London</span>
			</div>
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>July 2019</span>
			  <p className = 'biography__section__content__what_title'><strong>2019 Left Bank Leeds Art Prize</strong></p>
			  <p className = 'biography__section__content__what_title'>• I exhibited my photo ‘Live in Plastic”</p>
			  <span className = 'biography__section__content__where'>Leeds</span>
			</div>
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Apr 2019</span>
			  <p className = 'biography__section__content__what_title'><strong>Ourhistory Archives, Logic &amp; fabric present: Club Culture</strong></p>
			  <p className = 'biography__section__content__what_title'>• I exhibited my photo ‘Red Light’ curated by Red Gallery founder Ernesto Leal</p>
			  <span className = 'biography__section__content__where'>London</span>
			</div>
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Apr – May 2018</span>
			  <p className = 'biography__section__content__what_title'><strong>Sony World Photography Awards Exhibition at Somerset House</strong></p>
			  <p className = 'biography__section__content__what_title'>• My work ‘The Eye’ was selected from 129,338 images from 210 countries</p>
			  <span className = 'biography__section__content__where'>London</span>
			</div>
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Jan 2018</span>
			  <p className = 'biography__section__content__what_title'><strong>‘Verso altri lidi’ exhibition at FAX Cultural Association</strong></p>
			  <p className = 'biography__section__content__what_title'>• Personal exhibition: eight works edited and offered for sale</p>
			  <span className = 'biography__section__content__where'>Rome</span>
			</div>
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Nov 2017</span>
			  <p className = 'biography__section__content__what_title'><strong>‘Before sex’ at TAG, Tevere Art Gallery</strong></p>
			  <p className = 'biography__section__content__what_title'>• I exhibited my self-portatrit ‘Match Point’</p>
			  <span className = 'biography__section__content__where'>Rome</span>
			</div>
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Sep 2017</span>
			  <p className = 'biography__section__content__what_title'><strong>Live Cinema Festival for Macro, Museo d’Arte contemporanea di Roma</strong></p>
			  <p className = 'biography__section__content__what_title'>• I exhibited my video ‘De solitude’</p>
			  <span className = 'biography__section__content__where'>Rome</span>
			</div>
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>May 2017</span>
			  <p className = 'biography__section__content__what_title'><strong>MATRICE in the historical Pastificio Cerere of Rome</strong></p>
			  <p className = 'biography__section__content__what_title'>• I participated with a self-portrait called ‘Meet your Meat’</p>
			  <span className = 'biography__section__content__where'>Rome</span>
			</div>
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Oct 2016</span>
			  <p className = 'biography__section__content__what_title'><strong>Ri/velazioni for Rome Art Week</strong></p>
			  <p className = 'biography__section__content__what_title'>• I participated with three photographic works curated by Alessandra Addante</p>
			  <span className = 'biography__section__content__where'>Rome</span>
			</div>
			<div className = 'biography__section__content'>
			  <span className = 'biography__section__content__when'>Dec 2016</span>
			  <p className = 'biography__section__content__what_title'><strong>ET CETERA - Italian young street photography</strong></p>
			  <p className = 'biography__section__content__what_title'>• I participated with three photographic works curated by Eleutheria Foundation</p>
			  <span className = 'biography__section__content__where'>Prague</span>
			</div>
		  </section>
		</div>
	  </section>
	  <Footer />
	</>
  )
}

export default Biography;
