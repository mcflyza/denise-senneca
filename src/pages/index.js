import React from 'react'

import Header from '../components/shared/header/header'
import Footer from '../components/shared/footer/footer'
import ResponsiveImage from '../components/shared/responsiveImage/responsiveImage'
import HelmetComponent from '../components/shared/helmetComponenet'

import '../styles/index.scss'
import '../styles/pages/index/index.scss';

const Index = () => {
  let srcSetImgs = {
    smart:
      'https://res.cloudinary.com/mcflyza/image/upload/v1566523933/HomePage/HomePage1_Smart_ztmfpz.jpg',
    tabPort:
      'https://res.cloudinary.com/mcflyza/image/upload/v1566523934/HomePage/HomePage2_TabPort_fheoij.jpg',
    tabLand:
      'https://res.cloudinary.com/mcflyza/image/upload/v1566523937/HomePage/HomePage3_TabLand_btxdlu.jpg',
    desk:
      'https://res.cloudinary.com/mcflyza/image/upload/v1566523934/HomePage/HomePage4_Desk_zqjmj5.jpg',
    deskGr:
      'https://res.cloudinary.com/mcflyza/image/upload/v1566523933/HomePage/HomePage5_DeskGr_wbvpka.jpg',
    smartR:
      'https://res.cloudinary.com/mcflyza/image/upload/v1566523934/HomePage/HomePage6_SmartR_gqwehd.jpg',
    tabPortR:
      'https://res.cloudinary.com/mcflyza/image/upload/v1566523936/HomePage/HomePage7_TabPortR_gyrbyd.jpg',
    tabLandR:
      'https://res.cloudinary.com/mcflyza/image/upload/v1566523936/HomePage/HomePage8_TabLandR_l9kqdt.jpg',
    deskR:
      'https://res.cloudinary.com/mcflyza/image/upload/v1566523934/HomePage/HomePage9_DeskR_brv3os.jpg',
    deskGrR:
      'https://res.cloudinary.com/mcflyza/image/upload/v1566523936/HomePage/HomePage10_DeskGrR_cdorxo.jpg',
    altValue: 'black eyes',
  }

  return (
    <>
    <HelmetComponent />
    <Header />
      <div className='indexImage'>
        <ResponsiveImage className='imgHome' srcSetImgs={srcSetImgs} />
      </div>
      <Footer />
    </>
  )
}

export default Index
