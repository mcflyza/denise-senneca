import React from 'react'

import Header from '../components/shared/header/header'
import Footer from '../components/shared/footer/footer'
import AngelFaceCardComponent from '../components/shared/new-projects/angel-face/angelFaceCardComponent'
import LeHaideCardComponent from '../components/shared/new-projects/le-haine/leHaineCardComponent'
import RebeccaCardComponent from '../components/shared/new-projects/rebecca/rebeccaCardComponent'
import RecordingFlowersCardComponent from '../components/shared/new-projects/recording-flowers/recordingFlowersCardComponent'
import TheBlackSwanCardComponent from '../components/shared/new-projects/the-black-swan/theBlackSwanCardComponent'
import HelmetComponent from '../components/shared/helmetComponenet'

import '../styles/index.scss'
import '../styles/pages/new_projects/new_projects.scss';

const NewProjects = () => (
	<>
		<HelmetComponent />
		<Header />
		<div className='new_projects_cards_wrapper'>
			<AngelFaceCardComponent />
			<RecordingFlowersCardComponent />
			<RebeccaCardComponent />
			<LeHaideCardComponent />
			<TheBlackSwanCardComponent />
		</div>
		<Footer />
	</>
)

export default NewProjects