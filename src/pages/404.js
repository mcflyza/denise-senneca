import React from "react"
import Header from "../components/shared/header/header"

const NotFound = () => (
	<>
		<Header />
		<h2>NON HAI TROVATO NULLA</h2>
	</>
)

export default NotFound
