import React from "react"

import Header from "../../components/shared/header/header"
import Footer from "../../components/shared/footer/footer"
import LeHaineComponent from "../../components/shared/new-projects/le-haine/leHaineComponent"
import HelmetComponent from '../../components/shared/helmetComponenet'

const LeHaide = () => (
    <>
        <HelmetComponent /> 
        <Header />
        <LeHaineComponent />
        <Footer />
    </>
)

export default LeHaide
