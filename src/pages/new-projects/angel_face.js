import React from "react"

import Header from "../../components/shared/header/header"
import Footer from "../../components/shared/footer/footer"
import AngelFaceComponent from "../../components/shared/new-projects/angel-face/angelFaceComponent"
import HelmetComponent from '../../components/shared/helmetComponenet'

const AngelFace = () => (
	<>
		<HelmetComponent />
		<Header />
		<AngelFaceComponent />
		<Footer />
	</>
)

export default AngelFace
