import React from "react"

import Header from "../../components/shared/header/header"
import Footer from "../../components/shared/footer/footer"
import TheBlackSwainComponent from "../../components/shared/new-projects/the-black-swan/theBlackSwanComponent"
import HelmetComponent from '../../components/shared/helmetComponenet'

const LeHaide = () => (
    <>
        <HelmetComponent />
        <Header />
        <TheBlackSwainComponent />
        <Footer />
    </>
)

export default LeHaide