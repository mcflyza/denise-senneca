import React from "react"

import Header from "../../components/shared/header/header"
import Footer from "../../components/shared/footer/footer"
import RebeccaComponent from "../../components/shared/new-projects/rebecca/rebeccaComponent"
import HelmetComponent from '../../components/shared/helmetComponenet'

const LeHaide = () => (
    <>
        <HelmetComponent />
        <Header />
        <RebeccaComponent />
        <Footer />
    </>
)

export default LeHaide
