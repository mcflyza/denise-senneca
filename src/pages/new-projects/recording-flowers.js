import React from "react"

import Header from "../../components/shared/header/header"
import Footer from "../../components/shared/footer/footer"
import RecordingFlowersComponent from "../../components/shared/new-projects/recording-flowers/recordingFlowersComponent"
import HelmetComponent from '../../components/shared/helmetComponenet'

const LeHaide = () => (
    <>
        <HelmetComponent />
        <Header />
        <RecordingFlowersComponent />
        <Footer />
    </>
)

export default LeHaide
