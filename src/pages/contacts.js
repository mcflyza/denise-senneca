import React from 'react'

import Header from '../components/shared/header/header'
import Footer from '../components/shared/footer/footer'
import HelmetComponent from '../components/shared/helmetComponenet'

// get our fontawesome imports
import { 
    faMailBulk,
    faCamera,
    faBook
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import '../styles/index.scss'
import '../styles/pages/contacts/contacts.scss';

const Performances = () => (
    <>
        <HelmetComponent />
        <Header />
        <ul className='contacts__wrapper'>
            <li><div className='iconWrapper'><FontAwesomeIcon icon={faMailBulk} /></div>denise.senneca.uk@gmail.com</li>
            <div className='subContacts'>
                <li>
                    <a
                        href='https://www.instagram.com/denisesenneca/'
                        target='_blank'
                        rel="noopener noreferrer"
                    >
                        <div className='iconWrapper'><FontAwesomeIcon icon={faCamera} /></div>instagram
                    </a>
                </li>
                {/*
                                    <li>
                                        <a 
                                            href = 'https://www.facebook.com/sennecadenise' 
                                            target = '_blank' 
                                            rel="noopener noreferrer"
                                        >
                                            facebook
                                        </a>
                                    </li>
                                */}
                <li>
                    <a
                        href='https://www.facebook.com/Denise-Senneca-2146929055571114/'
                        target='_blank'
                        rel="noopener noreferrer"
                    >
                        <div className='iconWrapper'><FontAwesomeIcon icon={faBook} /></div>facebook
                    </a>
                </li>
            </div>
        </ul>
        <Footer />  
    </>
)

export default Performances
