import React from 'react'

import Header from '../components/shared/header/header'
import Footer from '../components/shared/footer/footer'
import BloodCardComponent from '../components/shared/performances/blood/bloodCardComponent'
import PleaseTellMeAndCryCardComponent from '../components/shared/performances/pleaseTellMeAndCry/pleaseTellMeAndCryCardComponent'
import TheActOfDestructionCardComponent from '../components/shared/performances/theActOfDestruction/theActOfDestructionCardComponent'
import HelmetComponent from '../components/shared/helmetComponenet'

import '../styles/index.scss'
import '../styles/pages/performances/performances.scss';

const Performances = () => (
  <>
    <HelmetComponent />
    <Header />
    <div className='performances_cards_wrapper'>
      <BloodCardComponent />
      <PleaseTellMeAndCryCardComponent />
      <TheActOfDestructionCardComponent />
    </div>
    <Footer />
  </>
)

export default Performances
