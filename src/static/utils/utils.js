export const titleToClass = (string) => string.toLowerCase().replace(/\s+/g, '');
