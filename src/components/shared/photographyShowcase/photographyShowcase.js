import React from 'react'
import ResponsiveImage from '../responsiveImage/responsiveImage'

import { titleToClass } from '../../../static/utils/utils'

const PhotographyShowcase = (props) => {
    let {
        titleImg,
        listImg
    } = props;

    return  <section className={`photography__section__${titleToClass(titleImg)}`}>
                <h5>{titleImg}</h5>
                <ResponsiveImage srcSetImgs = {listImg}/>
            </section>
}

export default PhotographyShowcase;
