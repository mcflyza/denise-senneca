import React from 'react'
import { Link } from 'gatsby'

import './menu.scss'

const showSubMenu = (evt) => {
    let subMenu = evt.target.querySelector('ul');
    if(subMenu != null) {
        subMenu.style.display = 'flex'
    }
}

const hideSubMenu = (evt) => {
    let subMenu = evt.target.querySelector('ul');
    if(subMenu != null) {
        subMenu.style.display = ''
    }
}

const Menu = () => (
    <>
        <input type = 'checkbox' id = 'overlay-input' />
        <label htmlFor = 'overlay-input' id = 'overlay-button'><span></span></label>
        <div id = 'overlay'>
            <nav className = 'menu'>
                <ul>
                    <li><Link to = '/biography/'>biography</Link></li>
                    <li onMouseEnter={showSubMenu} onMouseLeave={hideSubMenu}>
                        <Link to = '/photography/'>photography</Link>
                        <ul className = 'menu__photography'>
                            <li><Link to = '/photography/finearts'>fine arts</Link></li>
                            <li><Link to = '/photography/reportage'>reportage</Link></li>
                            <li><Link to = '/photography/street'>street</Link></li>
                            <li><Link to = '/photography/advertising'>advertising</Link></li>
                        </ul>
                    </li>
                    <li><Link to = '/performances/'>performances</Link></li>
                    <li><Link to= '/new-projects/'>new projects</Link></li>
                    <li onMouseEnter={showSubMenu} onMouseLeave={hideSubMenu}>
                        <Link to = '/contacts/'>contacts</Link>
                        <ul className = 'menu__contacts'>
                            <li>denise.senneca.uk@gmail.com</li>
                            <li>
                                <a 
                                    href = 'https://www.instagram.com/denisesenneca/' 
                                    target = '_blank' 
                                    rel="noopener noreferrer"
                                >
                                    instagram
                                </a>
                            </li>
                            {/*
                                <li>
                                    <a 
                                        href = 'https://www.facebook.com/sennecadenise' 
                                        target = '_blank' 
                                        rel="noopener noreferrer"
                                    >
                                        facebook
                                    </a>
                                </li>
                            */}
                            <li>
                                <a 
                                    href = 'https://www.facebook.com/Denise-Senneca-2146929055571114/' 
                                    target = '_blank' 
                                    rel="noopener noreferrer"
                                >
                                    facebook
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>
            </nav>
        </div>
    </>
)

export default Menu
