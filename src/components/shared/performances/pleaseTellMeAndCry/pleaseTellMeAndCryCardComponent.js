import React from 'react'
import PhotoCard from '../../photoCard/photoCard'

const PTMACCardInfo = {
    link: '/performances/please_tell_me_and_cry',
    title: 'Please Tell Me And Cry',
    srcSetImgs: {
        smart:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523955/Performance/PTMAC/Thumbnail/PTMACT1_Smart_pazbey.jpg',
        tabPort:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523956/Performance/PTMAC/Thumbnail/PTMACT2_TabPort_m1jteh.jpg',
        tabLand:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523960/Performance/PTMAC/Thumbnail/PTMACT3_TabLand_qdsyye.jpg',
        desk:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523960/Performance/PTMAC/Thumbnail/PTMACT4_Desk_kkznmo.jpg',
        deskGr:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523960/Performance/PTMAC/Thumbnail/PTMACT5_DeskGr_qv4elp.jpg',
        smartR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523958/Performance/PTMAC/Thumbnail/PTMACT6_SmartR_ew7zp6.jpg',
        tabPortR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523960/Performance/PTMAC/Thumbnail/PTMACT7_TabPortR_ychpnp.jpg',
        tabLandR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523958/Performance/PTMAC/Thumbnail/PTMACT8_TabLandR_kraec7.jpg',
        deskR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523955/Performance/PTMAC/Thumbnail/PTMACT9_Desk_nafsxh.jpg',
        deskGrR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523960/Performance/PTMAC/Thumbnail/PTMACT10_DeskGrR_rdceu8.jpg',
        altValue: 'Please Tell Me And Cry',
    },
}

const PleaseTellMeAndCryCardComponent = () => (
    <>
        <PhotoCard photoCardInfo={PTMACCardInfo} />
    </>
)

export default PleaseTellMeAndCryCardComponent
