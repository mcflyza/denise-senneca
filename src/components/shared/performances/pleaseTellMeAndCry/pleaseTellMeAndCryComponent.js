import React from 'react'

import PhotographySection from '../../photographySection/photographySection'

const PTMACInfo = {
    titleSection: 'Please Tell Me And Cry',
    photographyInfo: {
        'Please Tell Me And Cry 1': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523967/Performance/PTMAC/FirstPTMAC/FirstPTMAC1_Smart_ceu5bp.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523967/Performance/PTMAC/FirstPTMAC/FirstPTMAC2_TabPort_vjrfev.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523965/Performance/PTMAC/FirstPTMAC/FirstPTMAC3_TabLand_dxg0bk.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523965/Performance/PTMAC/FirstPTMAC/FirstPTMAC4_Desk_bu0mgl.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523964/Performance/PTMAC/FirstPTMAC/FirstPTMAC5_DeskGr_h5to8m.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523963/Performance/PTMAC/FirstPTMAC/FirstPTMAC6_SmartR_rhz5jp.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523964/Performance/PTMAC/FirstPTMAC/FirstPTMAC7_TabPortR_eon8it.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523967/Performance/PTMAC/FirstPTMAC/FirstPTMAC8_TabLandR_hkxxho.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523969/Performance/PTMAC/FirstPTMAC/FirstPTMAC9_DeskR_mbrtrh.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523967/Performance/PTMAC/FirstPTMAC/FirstPTMAC10_DeskGrR_nlwtlk.jpg',
            altValue: 'Please Tell Me And Cry 1'
        },
        'Please Tell Me And Cry 2': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523973/Performance/PTMAC/SecondPTMAC/SecondPTMAC1_Smart_beyv8o.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523969/Performance/PTMAC/SecondPTMAC/SecondPTMAC2_TabPort_lsrgb2.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523972/Performance/PTMAC/SecondPTMAC/SecondPTMAC3_TabLand_awkwm9.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523969/Performance/PTMAC/SecondPTMAC/SecondPTMAC4_Desk_ccfinq.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523971/Performance/PTMAC/SecondPTMAC/SecondPTMAC5_DeskGr_upmhfi.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523969/Performance/PTMAC/SecondPTMAC/SecondPTMAC6_SmartR_a0w9zj.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523969/Performance/PTMAC/SecondPTMAC/SecondPTMAC7_TabPortR_w5brnq.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523972/Performance/PTMAC/SecondPTMAC/SecondPTMAC8_TabLandR_zwencu.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523972/Performance/PTMAC/SecondPTMAC/SecondPTMAC9_DeskR_jzk3bp.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523971/Performance/PTMAC/SecondPTMAC/SecondPTMAC10_DeskGrR_csmaap.jpg',
            altValue: 'Please Tell Me And Cry 2'
        }, 
        'Please Tell Me And Cry 3': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523980/Performance/PTMAC/ThirdPTMAC/ThirdPTMAC1_Smart_oj4xkk.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523975/Performance/PTMAC/ThirdPTMAC/ThirdPTMAC2_TabPort_i1usms.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523977/Performance/PTMAC/ThirdPTMAC/ThirdPTMAC3_TabLand_atqd0j.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523978/Performance/PTMAC/ThirdPTMAC/ThirdPTMAC4_Desk_ylfzeg.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523977/Performance/PTMAC/ThirdPTMAC/ThirdPTMAC5_DeskGr_ougnxp.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523974/Performance/PTMAC/ThirdPTMAC/ThirdPTMAC6_SmartR_dwgms0.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523982/Performance/PTMAC/ThirdPTMAC/ThirdPTMAC7_TabPortR_djfwv9.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523978/Performance/PTMAC/ThirdPTMAC/ThirdPTMAC8_TabLandR_tweco0.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523977/Performance/PTMAC/ThirdPTMAC/ThirdPTMAC9_DeskR_db216x.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523977/Performance/PTMAC/ThirdPTMAC/ThirdPTMAC10_DeskGrR_u3ng6j.jpg',
            altValue: 'Please Tell Me And Cry 3'
        },
        'Please Tell Me And Cry 4': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523985/Performance/PTMAC/FourthPTMAC/FourthPTMAC1_Smart_e8sqmg.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523985/Performance/PTMAC/FourthPTMAC/FourthPTMAC2_TabPort_a20xxy.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523982/Performance/PTMAC/FourthPTMAC/FourthPTMAC3_TabLand_adbtuh.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523983/Performance/PTMAC/FourthPTMAC/FourthPTMAC4_Desk_ugiccu.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523982/Performance/PTMAC/FourthPTMAC/FourthPTMAC5_DeskGr_pgqdp1.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523982/Performance/PTMAC/FourthPTMAC/FourthPTMAC6_SmartR_mfhstd.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523982/Performance/PTMAC/FourthPTMAC/FourthPTMAC7_TabPortR_xpsbmn.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523982/Performance/PTMAC/FourthPTMAC/FourthPTMAC8_DeskGrR_hiym6w.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523985/Performance/PTMAC/FourthPTMAC/FourthPTMAC9_DeskR_na7m2t.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1569080653/Performance/PTMAC/FourthPTMAC/FourthPTMAC10_DeskGrR_zd6qtk.jpg',
            altValue: 'Please Tell Me And Cry 4'
        }
    }
}

const PleaseTellMeAndCryComponent = () => (
    <>
        <div className='PTMACWrapper'>
            <PhotographySection infoSection={PTMACInfo} />
        </div>
    </>
)

export default PleaseTellMeAndCryComponent;
