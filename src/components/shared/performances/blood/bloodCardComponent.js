import React from "react"
import PhotoCard from "../../photoCard/photoCard"

const bloodCardInfo = {
	link: "/performances/blood",
	title: "Blood",
	srcSetImgs: {
		smart:
		"https://res.cloudinary.com/mcflyza/image/upload/v1566523940/Performance/Blood/Thumbnail/BloodT1_Smart_oio320.jpg",
		tabPort:
		"https://res.cloudinary.com/mcflyza/image/upload/v1566523944/Performance/Blood/Thumbnail/BloodT2_TabPort_dxh5j0.jpg",
		tabLand:
		"https://res.cloudinary.com/mcflyza/image/upload/v1566523940/Performance/Blood/Thumbnail/BloodT3_TabLand_tqfoe6.jpg",
		desk:
		"https://res.cloudinary.com/mcflyza/image/upload/v1566523943/Performance/Blood/Thumbnail/BloodT4_Desk_cfe6zw.jpg",
		deskGr:
		"https://res.cloudinary.com/mcflyza/image/upload/v1566523940/Performance/Blood/Thumbnail/BloodT5_DeskGr_f6e10c.jpg",
		smartR:
		"https://res.cloudinary.com/mcflyza/image/upload/v1566523940/Performance/Blood/Thumbnail/BloodT6_SmartR_aykaq3.jpg",
		tabPortR:
		"https://res.cloudinary.com/mcflyza/image/upload/v1566523944/Performance/Blood/Thumbnail/BloodT7_TabPortR_fmwel2.jpg",
		tabLandR:
		"https://res.cloudinary.com/mcflyza/image/upload/v1566523944/Performance/Blood/Thumbnail/BloodT8_TabLandR_xya0ly.jpg",
		deskR:
		"https://res.cloudinary.com/mcflyza/image/upload/v1566523940/Performance/Blood/Thumbnail/BloodT9_DeskR_ffoeor.jpg",
		deskGrR:
		"https://res.cloudinary.com/mcflyza/image/upload/v1566523940/Performance/Blood/Thumbnail/BloodT10_DeskGrR_wjr1po.jpg",
		altValue: "Blood",
	},
}

const BloodComponent = () => (
	<>
		<PhotoCard photoCardInfo={bloodCardInfo} />
	</>
)

export default BloodComponent
