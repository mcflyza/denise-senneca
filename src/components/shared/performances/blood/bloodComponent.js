import React from 'react'

import PhotographySection from '../../photographySection/photographySection'

const bloodInfo = {
	titleSection: 'Blood',
	photographyInfo: {
		'Blood 1': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523948/Performance/Blood/FirstBlood/FirstBlood1_Smart_adclx3.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523951/Performance/Blood/FirstBlood/FirstBlood2_TabPort_k2popk.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523948/Performance/Blood/FirstBlood/FirstBlood3_TabLand_nfcpui.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523948/Performance/Blood/FirstBlood/FirstBlood4_Desk_cdc2jy.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523948/Performance/Blood/FirstBlood/FirstBlood5_DeskGr_rfbic0.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523946/Performance/Blood/FirstBlood/FirstBlood6_SmartR_qbyp34.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523952/Performance/Blood/FirstBlood/FirstBlood7_TabPortR_wmj8rz.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523951/Performance/Blood/FirstBlood/FirstBlood8_TabLandR_oha1di.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523948/Performance/Blood/FirstBlood/FirstBlood9_DeskR_qs3yeb.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523948/Performance/Blood/FirstBlood/FirstBlood10_DeskGrR_jtjt1z.jpg',
			altValue: 'Blood 1'
		}
	}
}

const BloodComponent = () => (
    <>
		<div className='bloodWrapper'>
			<PhotographySection infoSection = {bloodInfo} />
		</div>
    </>
)

export default BloodComponent;
