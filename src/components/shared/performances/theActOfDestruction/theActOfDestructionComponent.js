import React from 'react'

const TheActOfDestructionComponent = () => (
    <>
        <div className='TAODWrapper'>
            <section>
                <h3>The Act Of Destruction</h3>
                <video width="550" height="380" controls>
                    <source src="https://res.cloudinary.com/mcflyza/video/upload/v1569085870/Performance/TAOD/Video/The_Act_of_Destruction_qrlcjc.mp4" type="video/mp4" />
                    <source src="movie.ogg" type="video/ogg" />
                </video>
            </section>
        </div>
    </>
)

export default TheActOfDestructionComponent;
