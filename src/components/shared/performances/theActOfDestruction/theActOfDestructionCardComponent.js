import React from 'react'
import PhotoCard from '../../photoCard/photoCard'

const TAODCardInfo = {
    link: '/performances/the_act_of_destruction',
    title: 'The Act Of Destruction',
    srcSetImgs: {
        smart:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523987/Performance/TAOD/Thumbnail/TAODT1_Smart_jrx69l.jpg',
        tabPort:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523989/Performance/TAOD/Thumbnail/TAODT2_TabPort_ab3c5t.jpg',
        tabLand:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523987/Performance/TAOD/Thumbnail/TAODT3_TabLand_vjc43k.jpg',
        desk:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523986/Performance/TAOD/Thumbnail/TAODT4_Desk_r01scg.jpg',
        deskGr:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523989/Performance/TAOD/Thumbnail/TAODT5_DeskGr_yabyti.jpg',
        smartR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523985/Performance/TAOD/Thumbnail/TAODT6_SmarR_n7zllt.jpg',
        tabPortR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523990/Performance/TAOD/Thumbnail/TAODT7_TabPortR_vdskhj.jpg',
        tabLandR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523987/Performance/TAOD/Thumbnail/TAODT8_DeskR_hjond8.jpg',
        deskR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523989/Performance/TAOD/Thumbnail/TAODT9_TabLandR_pshsl6.jpg',
        deskGrR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1566523987/Performance/TAOD/Thumbnail/TAODT10_DeskGrR_qhospv.jpg',
        altValue: 'The Act Of Destruction',
    },
}

const TheActOfDestructionCardComponent = () => (
    <>
        <PhotoCard photoCardInfo={TAODCardInfo} />
    </>
)

export default TheActOfDestructionCardComponent
