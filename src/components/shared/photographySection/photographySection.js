import React from 'react'

import PhotographyShowcase from '../photographyShowcase/photographyShowcase' 

import { titleToClass } from '../../../static/utils/utils'

const PhotographySection = (props) => {
    let title = props.infoSection.titleSection;
    let titleForClass = titleToClass(title);
    let photographyInfo = Object.entries(props.infoSection.photographyInfo);

    let photographyInfoComponent = [];

    for (const [titleImg, listImg] of photographyInfo) {
        let wrapperName = titleToClass(titleImg);

        let photographyShowcaseWrapper =
            <div className = {`photography__${titleForClass}__content__${wrapperName}`}>
                <PhotographyShowcase key={titleImg} titleImg={titleImg} listImg = {listImg}/>
            </div>
        photographyInfoComponent.push(photographyShowcaseWrapper);
    }

    return (
        <section className = {`photography__${titleForClass}`}>
            <h3 className = {`photography__${titleForClass}__title`}>{title}</h3>
            <div className = {`photography__${titleForClass}__content`}>
                {photographyInfoComponent}
            </div>
        </section>
    )
}

export default PhotographySection;
