import React from "react"
import { Link } from 'gatsby'

import './header.scss'

import Menu from '../menu/menu'

const Header = () => (
    <header className='mainHeader'>
        <Link to='/'>
            <h1>Denise Senneca</h1>
        </Link>
        <Menu />
    </header>
)

export default Header
