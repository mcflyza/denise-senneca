import React from 'react'
import PhotoCard from '../../photoCard/photoCard'

const leHaineCardInfo = {
    link: '/new-projects/le_haine',
    title: 'Le Haine',
    srcSetImgs: {
        smart:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362512/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/01Smart_lwrj30.jpg',
        tabPort:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362512/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/02TabPort_u2wydt.jpg',
        tabLand:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362511/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/03TabLand_dahihz.jpg',
        desk:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362511/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/04Desk_mzn6xk.jpg',
        deskGr:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362512/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/05DeskGr_v4knbp.jpg',
        smartR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362511/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/06SmartR_e6voly.jpg',
        tabPortR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362512/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/07TabPort_mhrwzq.jpg',
        tabLandR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362511/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/08TabLand_mhsekd.jpg',
        deskR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362512/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/09DeskR_kemqin.jpg',
        deskGrR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362512/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/10DeskGrR_jpzsyt.jpg',
        altValue: 'Le Haine',
    },
}

const LeHaineCardComponent = () => <PhotoCard photoCardInfo={leHaineCardInfo} />

export default LeHaineCardComponent;
