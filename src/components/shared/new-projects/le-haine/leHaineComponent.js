import React from 'react'
import PhotographySection from '../../photographySection/photographySection'

const leHaineInfo = {
    titleSection: 'Le Haine',
    photographyInfo: {
        'Le Haine 1': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362512/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/01Smart_lwrj30.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362512/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/02TabPort_u2wydt.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362511/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/03TabLand_dahihz.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362511/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/04Desk_mzn6xk.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362512/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/05DeskGr_v4knbp.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362511/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/06SmartR_e6voly.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362512/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/07TabPort_mhrwzq.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362511/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/08TabLand_mhsekd.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362512/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/09DeskR_kemqin.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362512/NewProjects/LE_HAINE/LE_HAINE_1_THUMB/10DeskGrR_jpzsyt.jpg',
            altValue: 'Le Haine 1',
        },
        'Le Haine 2': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362508/NewProjects/LE_HAINE/LE_HAINE_2/01Smart_g48gwl.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362508/NewProjects/LE_HAINE/LE_HAINE_2/02TabPort_i6usfm.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362506/NewProjects/LE_HAINE/LE_HAINE_2/03TabLand_jojiw2.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362507/NewProjects/LE_HAINE/LE_HAINE_2/04Desk_f23nbo.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362509/NewProjects/LE_HAINE/LE_HAINE_2/05DeskGr_c7vkpo.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362507/NewProjects/LE_HAINE/LE_HAINE_2/06SmartR_scjwpj.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362507/NewProjects/LE_HAINE/LE_HAINE_2/07TabLPortR_x0cg2s.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362507/NewProjects/LE_HAINE/LE_HAINE_2/08TabLandR_rs8t66.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362508/NewProjects/LE_HAINE/LE_HAINE_2/09DeskR_a6jkoa.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362508/NewProjects/LE_HAINE/LE_HAINE_2/10DeskGrR_zldhqx.jpg',
            altValue: 'Le Haine 2'
        },
        'Le Haine 3': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362508/NewProjects/LE_HAINE/LE_HAINE_3/01Smart_mqvmgx.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362508/NewProjects/LE_HAINE/LE_HAINE_3/02TabPort_yjwune.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362505/NewProjects/LE_HAINE/LE_HAINE_3/03TabLand_hvyciv.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362506/NewProjects/LE_HAINE/LE_HAINE_3/04Desk_aopjef.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362509/NewProjects/LE_HAINE/LE_HAINE_3/05DeskGr_j4txzg.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362507/NewProjects/LE_HAINE/LE_HAINE_3/06SmartR_atsijl.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362507/NewProjects/LE_HAINE/LE_HAINE_3/07TabPortR_to0m1b.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362506/NewProjects/LE_HAINE/LE_HAINE_3/08TabLandR_afnzoq.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362508/NewProjects/LE_HAINE/LE_HAINE_3/09DeskR_sjtdhe.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362507/NewProjects/LE_HAINE/LE_HAINE_3/10DeskGrR_ubm1tc.jpg',
            altValue: 'Le Haine 3'
        },
        'Le Haine 4': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362510/NewProjects/LE_HAINE/LE_HAINE_4/01Smart_oajvo4.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362511/NewProjects/LE_HAINE/LE_HAINE_4/02TabPort_nqugj6.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362509/NewProjects/LE_HAINE/LE_HAINE_4/03TabLand_jt3la8.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362509/NewProjects/LE_HAINE/LE_HAINE_4/04Desk_jfnqaq.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362511/NewProjects/LE_HAINE/LE_HAINE_4/05DeskGr_gjh4ng.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362509/NewProjects/LE_HAINE/LE_HAINE_4/06SmartR_s1hv63.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362509/NewProjects/LE_HAINE/LE_HAINE_4/07TabLandR_wn6sr3.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362509/NewProjects/LE_HAINE/LE_HAINE_4/08TabPortR_y2htxe.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362510/NewProjects/LE_HAINE/LE_HAINE_4/09DeskR_xfzyho.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362510/NewProjects/LE_HAINE/LE_HAINE_4/10DeskGrR_t8sq9q.jpg',
            altValue: 'Le Haine 4'
        },
        'Le Haine 5': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362505/NewProjects/LE_HAINE/LE_HAINE_5/01Smart_jlq3hn.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362506/NewProjects/LE_HAINE/LE_HAINE_5/02TabPort_gnt5za.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362511/NewProjects/LE_HAINE/LE_HAINE_5/03TabLand_n1mh8r.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362511/NewProjects/LE_HAINE/LE_HAINE_5/04Desk_x0hroq.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362506/NewProjects/LE_HAINE/LE_HAINE_5/05DeskGr_pm0mmw.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362505/NewProjects/LE_HAINE/LE_HAINE_5/06SmartR_ghfvot.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362505/NewProjects/LE_HAINE/LE_HAINE_5/07TabPortR_mvk0su.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362512/NewProjects/LE_HAINE/LE_HAINE_5/08TabLandR_ytkdvl.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362506/NewProjects/LE_HAINE/LE_HAINE_5/09DeskR_afj8z4.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362506/NewProjects/LE_HAINE/LE_HAINE_5/10DeskGrR_fwzgwn.jpg',
            altValue: 'Le Haine 5'
        },
        'Le Haine 6': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362510/NewProjects/LE_HAINE/LE_HAINE_6/01Smart_u7rnh8.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362510/NewProjects/LE_HAINE/LE_HAINE_6/02TabPort_ltoapt.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362509/NewProjects/LE_HAINE/LE_HAINE_6/03TabLand_son8kl.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362509/NewProjects/LE_HAINE/LE_HAINE_6/04Desk_ezfvwj.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362510/NewProjects/LE_HAINE/LE_HAINE_6/05DeskGr_tmzpfv.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362509/NewProjects/LE_HAINE/LE_HAINE_6/06SmartR_nuqeqm.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362510/NewProjects/LE_HAINE/LE_HAINE_6/07TabPortR_kongpd.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362509/NewProjects/LE_HAINE/LE_HAINE_6/08TabLandR_ek9lkm.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362510/NewProjects/LE_HAINE/LE_HAINE_6/09DeskR_iprarl.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362510/NewProjects/LE_HAINE/LE_HAINE_6/10DeskGrR_bigq6u.jpg',
            altValue: 'Le Haine 6'
        }
    }
}

const LeHaineComponent = () => (
    <div className='leHaineWrapper'>
        <PhotographySection infoSection={leHaineInfo} />
    </div>
)

export default LeHaineComponent;
