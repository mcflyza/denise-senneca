import React from 'react'
import PhotoCard from '../../photoCard/photoCard'

const theBlackSwanCardInfo = {
    link: '/new-projects/the-black-swan',
    title: 'The Black Swan',
    srcSetImgs: {
        smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828082/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/01Smart_ftytks.jpg',
        tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828082/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/02Tab-Port_ihn9ql.jpg',
        tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828082/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/03Tab-Land_nyoylf.jpg',
        desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828082/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/04Desk_g4vmry.jpg',
        deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828082/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/05Desk-Gr_hnl79k.jpg',
        smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828082/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/06Smart-DisplayR_yylx6q.jpg',
        tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828082/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/07Tab-Port-DisplayR_ejvepc.jpg',
        tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828083/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/08Tab-Land-DisplayR_fujxdo.jpg',
        deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828083/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/09Desk-DisplayR_oayd7c.jpg',
        deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828083/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/10Desk-Gr-DisplayR_tfr08g.jpg',
        altValue: 'The Black Swan 6'
    }
}

const TheBlackSwanCardComponent = () => <PhotoCard photoCardInfo={theBlackSwanCardInfo} />

export default TheBlackSwanCardComponent;
