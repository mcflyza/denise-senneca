import React from 'react'
import PhotographySection from '../../photographySection/photographySection'

const theBlackSwanInfo = {
    titleSection: 'The Black Swan',
    photographyInfo: {
        'The Black Swan 1': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827638/NewProjects/THE_BLACK_SWAN/01TheBlackSwan/01Smart_dk7cjq.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827638/NewProjects/THE_BLACK_SWAN/01TheBlackSwan/02Tab-Port_giwp54.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827638/NewProjects/THE_BLACK_SWAN/01TheBlackSwan/03Tab-Land_tlcxpv.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827638/NewProjects/THE_BLACK_SWAN/01TheBlackSwan/04Desk_pojxwu.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827639/NewProjects/THE_BLACK_SWAN/01TheBlackSwan/05Desk-Gr_mi8r3c.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827639/NewProjects/THE_BLACK_SWAN/01TheBlackSwan/06Smart-DisplayR_jfk8ok.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827639/NewProjects/THE_BLACK_SWAN/01TheBlackSwan/07Tab-Port-DisplayR_lsaapt.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827639/NewProjects/THE_BLACK_SWAN/01TheBlackSwan/08Tab-Land-DisplayR_ee1na3.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827639/NewProjects/THE_BLACK_SWAN/01TheBlackSwan/09Desk-Display_n8ap2i.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827640/NewProjects/THE_BLACK_SWAN/01TheBlackSwan/10Desk-Gr-DisplayR_j0ajqx.jpg',
            altValue: 'The Black Swan 1',
        },
        'The Black Swan 2': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827955/NewProjects/THE_BLACK_SWAN/02TheBlackSwan/01Smart_mm4ejp.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827955/NewProjects/THE_BLACK_SWAN/02TheBlackSwan/02Tab-Port_e6iumn.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827955/NewProjects/THE_BLACK_SWAN/02TheBlackSwan/03Tab-Land_wmolub.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827956/NewProjects/THE_BLACK_SWAN/02TheBlackSwan/04Desk_lthy0t.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827956/NewProjects/THE_BLACK_SWAN/02TheBlackSwan/05Desk-Gr_jlovub.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827956/NewProjects/THE_BLACK_SWAN/02TheBlackSwan/06Smart_aroaf8.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827956/NewProjects/THE_BLACK_SWAN/02TheBlackSwan/07Smart-Port-DisplayR_bpclfl.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827956/NewProjects/THE_BLACK_SWAN/02TheBlackSwan/08Tab-Land-DisplayR_pb4l9s.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827957/NewProjects/THE_BLACK_SWAN/02TheBlackSwan/09Desk-DisplayR_mizbwe.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827957/NewProjects/THE_BLACK_SWAN/02TheBlackSwan/10Desk-Gr-DisplayR_aapauk.jpg',
            altValue: 'The Black Swan 2'
        },
        'The Black Swan 3': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827971/NewProjects/THE_BLACK_SWAN/03TheBlackSwan/01Smart_d4qllc.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827971/NewProjects/THE_BLACK_SWAN/03TheBlackSwan/02Tab-Port_yzw1bl.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827971/NewProjects/THE_BLACK_SWAN/03TheBlackSwan/03Tab-Land_gl4vip.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827971/NewProjects/THE_BLACK_SWAN/03TheBlackSwan/04Desk_buslmd.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827972/NewProjects/THE_BLACK_SWAN/03TheBlackSwan/05Desk-Gr_x7qkqg.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827972/NewProjects/THE_BLACK_SWAN/03TheBlackSwan/06Smart-DisplayR_zq5muc.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827972/NewProjects/THE_BLACK_SWAN/03TheBlackSwan/07Tab-Port-DisplayR_ciayyl.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827972/NewProjects/THE_BLACK_SWAN/03TheBlackSwan/08Tab-Land-DisplayR_c4kwqn.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827972/NewProjects/THE_BLACK_SWAN/03TheBlackSwan/09Desk-DisplayR_sclblr.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827973/NewProjects/THE_BLACK_SWAN/03TheBlackSwan/10Desk-Gr-DisplayR_pzr7ti.jpg',
            altValue: 'The Black Swan 3',
        },
        'The Black Swan 4': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827984/NewProjects/THE_BLACK_SWAN/04TheBlackSwan/01Smart_zgb7xi.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827984/NewProjects/THE_BLACK_SWAN/04TheBlackSwan/02Tab-Port_kuaekr.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827984/NewProjects/THE_BLACK_SWAN/04TheBlackSwan/03Tab-Land_yv5wfr.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827984/NewProjects/THE_BLACK_SWAN/04TheBlackSwan/04Desk_zaxadv.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827984/NewProjects/THE_BLACK_SWAN/04TheBlackSwan/05Desk-Gr_gs7jld.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827985/NewProjects/THE_BLACK_SWAN/04TheBlackSwan/06Smart-DisplayR_p9ttaj.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827985/NewProjects/THE_BLACK_SWAN/04TheBlackSwan/07Tab-Port-DisplayR_vamgms.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827985/NewProjects/THE_BLACK_SWAN/04TheBlackSwan/08Tab-Land-DisplayR_aryhmo.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827985/NewProjects/THE_BLACK_SWAN/04TheBlackSwan/09Desk-DisplayR_miyxgf.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587827986/NewProjects/THE_BLACK_SWAN/04TheBlackSwan/10Desk-Gr-DisplayR_h4ozjt.jpg',
            altValue: 'The Black Swan 4'
        },
        'The Black Swan 5': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828070/NewProjects/THE_BLACK_SWAN/05TheBlackSwan/01Smart_n2pzkl.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828070/NewProjects/THE_BLACK_SWAN/05TheBlackSwan/02Tab-Port_hmifsp.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828070/NewProjects/THE_BLACK_SWAN/05TheBlackSwan/03Tab-Land_enal0y.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828071/NewProjects/THE_BLACK_SWAN/05TheBlackSwan/04Desk_ukrjua.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828071/NewProjects/THE_BLACK_SWAN/05TheBlackSwan/05Desk-Gr_oszbfp.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828071/NewProjects/THE_BLACK_SWAN/05TheBlackSwan/06Smart-DisplayR_jubyja.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828071/NewProjects/THE_BLACK_SWAN/05TheBlackSwan/07Tab-Port-DisplayR_tahkcv.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828071/NewProjects/THE_BLACK_SWAN/05TheBlackSwan/08Tab-Land-DisplayR_sbiyyd.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828072/NewProjects/THE_BLACK_SWAN/05TheBlackSwan/09Desk-DisplayR_avcqiw.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828072/NewProjects/THE_BLACK_SWAN/05TheBlackSwan/10Desk-Gr-DisplayR_yklrx8.jpg',
            altValue: 'The Black Swan 5'
        },
        'The Black Swan 6': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828082/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/01Smart_ftytks.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828082/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/02Tab-Port_ihn9ql.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828082/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/03Tab-Land_nyoylf.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828082/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/04Desk_g4vmry.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828082/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/05Desk-Gr_hnl79k.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828082/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/06Smart-DisplayR_yylx6q.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828082/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/07Tab-Port-DisplayR_ejvepc.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828083/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/08Tab-Land-DisplayR_fujxdo.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828083/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/09Desk-DisplayR_oayd7c.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828083/NewProjects/THE_BLACK_SWAN/06TheBlackSwan/10Desk-Gr-DisplayR_tfr08g.jpg',
            altValue: 'The Black Swan 6'
        },
        'The Black Swan 7': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828101/NewProjects/THE_BLACK_SWAN/07TheBlackSwan/01Smart_pk6dq2.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828102/NewProjects/THE_BLACK_SWAN/07TheBlackSwan/02Tab-Port_knmxwv.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828102/NewProjects/THE_BLACK_SWAN/07TheBlackSwan/03Tab-Land_ep6rqp.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828102/NewProjects/THE_BLACK_SWAN/07TheBlackSwan/04Desk_fpfhkg.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828102/NewProjects/THE_BLACK_SWAN/07TheBlackSwan/05Desk-Gr_toxnip.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828102/NewProjects/THE_BLACK_SWAN/07TheBlackSwan/06Smart-DisplayR_lnxtcu.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828102/NewProjects/THE_BLACK_SWAN/07TheBlackSwan/07Tab-Port-DisplayR_xxaao4.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828103/NewProjects/THE_BLACK_SWAN/07TheBlackSwan/08Tab-Land-DisplayR_njdbtn.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828103/NewProjects/THE_BLACK_SWAN/07TheBlackSwan/09Desk-DisplayR_yusvdm.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828103/NewProjects/THE_BLACK_SWAN/07TheBlackSwan/10Desk-Gr-DisplayR_zwv1ih.jpg',
            altValue: 'The Black Swan 7'
        },
        'The Black Swan 8': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828119/NewProjects/THE_BLACK_SWAN/08TheBlackSwan/01Smart_pysjbv.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828119/NewProjects/THE_BLACK_SWAN/08TheBlackSwan/02Tab-Port_wvjeda.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828119/NewProjects/THE_BLACK_SWAN/08TheBlackSwan/03Tab-Land_mzpjde.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828119/NewProjects/THE_BLACK_SWAN/08TheBlackSwan/04Desk_b7hzbs.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828120/NewProjects/THE_BLACK_SWAN/08TheBlackSwan/05Desk-Gr_bv0moz.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828120/NewProjects/THE_BLACK_SWAN/08TheBlackSwan/06Smart-DisplayR_nig0is.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828120/NewProjects/THE_BLACK_SWAN/08TheBlackSwan/07Tab-Port-DisplayR_peimkw.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828120/NewProjects/THE_BLACK_SWAN/08TheBlackSwan/08Tab-Land-DisplayR_cer0jw.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828120/NewProjects/THE_BLACK_SWAN/08TheBlackSwan/09Desk-DisplayR_hyes5x.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587828121/NewProjects/THE_BLACK_SWAN/08TheBlackSwan/10Desk-Gr-DisplayR_zilaa5.jpg',
            altValue: 'The Black Swan 8'
        }
    }
}

const TheBlackSwanComponent = () => (
    <div className='theBlackSwanWrapper'>
        <PhotographySection infoSection={theBlackSwanInfo} />
    </div>
)

export default TheBlackSwanComponent;
