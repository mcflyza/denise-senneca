import React from 'react'
import PhotographySection from '../../photographySection/photographySection'

const rebeccaInfo = {
    titleSection: 'Rebecca',
    photographyInfo: {
        'Rebecca 1': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362514/NewProjects/REBECCA/REBECCA_1/01Smart_hosdsk.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362514/NewProjects/REBECCA/REBECCA_1/02TabPort_rlvl3y.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362513/NewProjects/REBECCA/REBECCA_1/03TabLand_npm0nr.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362514/NewProjects/REBECCA/REBECCA_1/04Desk_nfuozv.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362514/NewProjects/REBECCA/REBECCA_1/05DeskGr_nnajwq.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362514/NewProjects/REBECCA/REBECCA_1/06SmartR_awxi6k.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362514/NewProjects/REBECCA/REBECCA_1/07TabPortR_cb2eyf.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362514/NewProjects/REBECCA/REBECCA_1/08TabLandR_vstxhg.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362514/NewProjects/REBECCA/REBECCA_1/09DeskR_bpsddo.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362514/NewProjects/REBECCA/REBECCA_1/10DeskGrR_odueuw.jpg',
            altValue: 'Rebecca 1',
        },
        'Rebecca 2': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362517/NewProjects/REBECCA/REBECCA_2/01Smart_bw3zw0.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362518/NewProjects/REBECCA/REBECCA_2/02TabPort_jwlzyi.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362517/NewProjects/REBECCA/REBECCA_2/03TabLand_x76m6o.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362517/NewProjects/REBECCA/REBECCA_2/04Desk_ahgtpw.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362518/NewProjects/REBECCA/REBECCA_2/05DeskGr_clq9ax.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362517/NewProjects/REBECCA/REBECCA_2/06SmartR_tmp5dy.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362518/NewProjects/REBECCA/REBECCA_2/07TabPortR_dmaw0l.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362517/NewProjects/REBECCA/REBECCA_2/08TabLandR_rivyeq.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362517/NewProjects/REBECCA/REBECCA_2/09DeskR_kd2hkd.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362517/NewProjects/REBECCA/REBECCA_2/10DeskGrR_tm9wnu.jpg',
            altValue: 'Rebecca 2'
        },
        'Rebecca 3': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362515/NewProjects/REBECCA/REBECCA_3_THUMB/01Smart_x41sfv.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362515/NewProjects/REBECCA/REBECCA_3_THUMB/02TabPort_fhvb4l.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362514/NewProjects/REBECCA/REBECCA_3_THUMB/03TabLand_tufnki.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362514/NewProjects/REBECCA/REBECCA_3_THUMB/04Desk_krez56.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362516/NewProjects/REBECCA/REBECCA_3_THUMB/05DeskGr_jvdcxv.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362515/NewProjects/REBECCA/REBECCA_3_THUMB/06SmartR_jh0deo.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362515/NewProjects/REBECCA/REBECCA_3_THUMB/07TabPortR_rdzltw.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362515/NewProjects/REBECCA/REBECCA_3_THUMB/08TabLandR_uzwsth.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362515/NewProjects/REBECCA/REBECCA_3_THUMB/09DeskR_asq7fo.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362515/NewProjects/REBECCA/REBECCA_3_THUMB/10DeskGrR_obavjk.jpg',
            altValue: 'Rebecca',
        },
        'Rebecca 4': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362516/NewProjects/REBECCA/REBECCA_4/01Smart_bg0f7m.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362516/NewProjects/REBECCA/REBECCA_4/02TabPort_zys2mn.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362516/NewProjects/REBECCA/REBECCA_4/03TabLand_laoxe0.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362516/NewProjects/REBECCA/REBECCA_4/04Desk_s9dnpi.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362517/NewProjects/REBECCA/REBECCA_4/05DeskGr_gqp7rz.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362516/NewProjects/REBECCA/REBECCA_4/06SmartR_kcjcun.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362516/NewProjects/REBECCA/REBECCA_4/07TabPortR_yjbt4r.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362516/NewProjects/REBECCA/REBECCA_4/08TabLandR_jyps3t.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362516/NewProjects/REBECCA/REBECCA_4/09DeskR_wnxdaq.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362516/NewProjects/REBECCA/REBECCA_4/10DeskR_mh0gao.jpg',
            altValue: 'Rebecca 4'
        },
        'Rebecca 5': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362518/NewProjects/REBECCA/REBECCA_5/01Smart_khzp9d.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362519/NewProjects/REBECCA/REBECCA_5/02TabPort_ahqlzn.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362518/NewProjects/REBECCA/REBECCA_5/03TabLand_ep1ftj.jpg',
            desk: 'hhttps://res.cloudinary.com/mcflyza/image/upload/v1570362518/NewProjects/REBECCA/REBECCA_5/04Desk_nbdlid.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362519/NewProjects/REBECCA/REBECCA_5/05DeskGr_vx5ca6.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362518/NewProjects/REBECCA/REBECCA_5/06SmartR_yefyk0.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362519/NewProjects/REBECCA/REBECCA_5/07TabPortR_edip3y.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362518/NewProjects/REBECCA/REBECCA_5/08TabLandR_vujecy.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362518/NewProjects/REBECCA/REBECCA_5/09DeskR_djbyuw.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362518/NewProjects/REBECCA/REBECCA_5/10DeskGrR_lkob1c.jpg',
            altValue: 'Rebecca 5'
        },
        'Rebecca 6': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362513/NewProjects/REBECCA/REBECCA_6/01Smart_qvhrwx.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362513/NewProjects/REBECCA/REBECCA_6/02TabPort_vpspih.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362512/NewProjects/REBECCA/REBECCA_6/03TabLand_hzzoas.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362513/NewProjects/REBECCA/REBECCA_6/04Desk_s0whsf.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362513/NewProjects/REBECCA/REBECCA_6/05DeskGr_or0wfk.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362513/NewProjects/REBECCA/REBECCA_6/06SmartR_sfkf6q.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362513/NewProjects/REBECCA/REBECCA_6/07TabPortR_pmnfzz.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362513/NewProjects/REBECCA/REBECCA_6/08TabLandR_enbxpm.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362513/NewProjects/REBECCA/REBECCA_6/09DeskR_wcb8vc.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362513/NewProjects/REBECCA/REBECCA_6/10DeskGrR_xslhso.jpg',
            altValue: 'Rebecca 6'
        }
    }
}

const RebeccaComponent = () => (
    <div className='rebeccaWrapper'>
        <PhotographySection infoSection={rebeccaInfo} />
    </div>
)

export default RebeccaComponent;
