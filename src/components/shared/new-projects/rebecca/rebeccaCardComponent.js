import React from 'react'
import PhotoCard from '../../photoCard/photoCard'

const rebeccaCardInfo = {
    link: '/new-projects/rebecca',
    title: 'Rebecca',
    srcSetImgs: {
        smart:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362515/NewProjects/REBECCA/REBECCA_3_THUMB/01Smart_x41sfv.jpg',
        tabPort:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362515/NewProjects/REBECCA/REBECCA_3_THUMB/02TabPort_fhvb4l.jpg',
        tabLand:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362514/NewProjects/REBECCA/REBECCA_3_THUMB/03TabLand_tufnki.jpg',
        desk:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362514/NewProjects/REBECCA/REBECCA_3_THUMB/04Desk_krez56.jpg',
        deskGr:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362516/NewProjects/REBECCA/REBECCA_3_THUMB/05DeskGr_jvdcxv.jpg',
        smartR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362515/NewProjects/REBECCA/REBECCA_3_THUMB/06SmartR_jh0deo.jpg',
        tabPortR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362515/NewProjects/REBECCA/REBECCA_3_THUMB/07TabPortR_rdzltw.jpg',
        tabLandR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362515/NewProjects/REBECCA/REBECCA_3_THUMB/08TabLandR_uzwsth.jpg',
        deskR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362515/NewProjects/REBECCA/REBECCA_3_THUMB/09DeskR_asq7fo.jpg',
        deskGrR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362515/NewProjects/REBECCA/REBECCA_3_THUMB/10DeskGrR_obavjk.jpg',
        altValue: 'Rebecca',
    },
}

const RebeccaCardComponent = () => <PhotoCard photoCardInfo={rebeccaCardInfo} />

export default RebeccaCardComponent;
