import React from 'react'
import PhotoCard from '../../photoCard/photoCard'

const angelFaceCardInfo = {
	link: '/new-projects/angel_face',
	title: 'Angel Face',
	srcSetImgs: {
		smart:
		'https://res.cloudinary.com/mcflyza/image/upload/v1566523901/Blog/Thumbnail/AngelFace1_Smart_xr9abj.jpg',
		tabPort:
		'https://res.cloudinary.com/mcflyza/image/upload/v1566523901/Blog/Thumbnail/AngelFace2_TabPort_auyklk.jpg',
		tabLand:
		'https://res.cloudinary.com/mcflyza/image/upload/v1566523900/Blog/Thumbnail/AngelFace3_TabLand_zjkfwx.jpg',
		desk:
		'https://res.cloudinary.com/mcflyza/image/upload/v1566523896/Blog/Thumbnail/AngelFace4_Desk_uldkae.jpg',
		deskGr:
		'https://res.cloudinary.com/mcflyza/image/upload/v1566523901/Blog/Thumbnail/AngelFace5_DeskGr_fr58cg.jpg',
		smartR:
		'https://res.cloudinary.com/mcflyza/image/upload/v1566523901/Blog/Thumbnail/AngelFace6_SmartR_f1quax.jpg',
		tabPortR:
		'https://res.cloudinary.com/mcflyza/image/upload/v1566523905/Blog/Thumbnail/AngelFace7_TabPortR_np1bi2.jpg',
		tabLandR:
		'https://res.cloudinary.com/mcflyza/image/upload/v1566523901/Blog/Thumbnail/AngelFace8_TabLandR_zjfqi1.jpg',
		deskR:
		'https://res.cloudinary.com/mcflyza/image/upload/v1566523897/Blog/Thumbnail/AngelFace9_DeskR_frlz7f.jpg',
		deskGrR:
		'https://res.cloudinary.com/mcflyza/image/upload/v1566523900/Blog/Thumbnail/AngelFace10_DeskGrR_nbqogt.jpg',
		altValue: 'Angel Face',
	},
}

const AngelFaceCardComponent = () => <PhotoCard photoCardInfo={angelFaceCardInfo} />

export default AngelFaceCardComponent;
