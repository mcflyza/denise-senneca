import React from 'react'
import PhotographySection from '../../photographySection/photographySection'

const angelFaceInfo = {
	titleSection: 'Angel Face',
	photographyInfo: {
		'Angel Face 1': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523920/Blog/FirstAngelFace/FirstAngelFace1_Smart_x2fscs.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523923/Blog/FirstAngelFace/FirstAngelFace2_TabPort_ocwits.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523930/Blog/FirstAngelFace/FirstAngelFace3_TabLand_aat3ov.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523930/Blog/FirstAngelFace/FirstAngelFace4_Desk_jv6tey.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523930/Blog/FirstAngelFace/FirstAngelFace5_DeskGr_s78xm2.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523926/Blog/FirstAngelFace/FirstAngelFace6_SmartR_uhfxuk.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523925/Blog/FirstAngelFace/FirstAngelFace7_TabLandR_rgnxkr.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523930/Blog/FirstAngelFace/FirstAngelFace8_TabPortR_y9mejd.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523923/Blog/FirstAngelFace/FirstAngelFace9_DeskR_dbslpc.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523923/Blog/FirstAngelFace/FirstAngelFace10_DeskGr_bejitu.jpg',
			altValue: 'Angel Face 1'
		},
		'Angel Face 2': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523914/Blog/SecondAngelFace/SecondAngelFace1_Smart_jud7hm.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523911/Blog/SecondAngelFace/SecondAngelFace2_TabPort_raddja.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523912/Blog/SecondAngelFace/SecondAngelFace3_TabLand_l387qv.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523915/Blog/SecondAngelFace/SecondAngelFace4_Desk_qy55db.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523915/Blog/SecondAngelFace/SecondAngelFace5_DeskGr_rmqeuu.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523919/Blog/SecondAngelFace/SecondAngelFace6_SmartR_ltok2t.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523917/Blog/SecondAngelFace/SecondAngelFace7_TabPortR_btsv90.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523915/Blog/SecondAngelFace/SecondAngelFace8_TabLandR_imivhl.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523920/Blog/SecondAngelFace/SecondAngelFace9_DeskR_y2wyac.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523911/Blog/SecondAngelFace/SecondAngelFace10_DeskGrR_rp4vlk.jpg',
			altValue: 'Angel Face 2'
		},
		'Angel Face 3': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523906/Blog/ThirdAngelFace/ThirdAngelFace1_Smart_vox6u6.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523911/Blog/ThirdAngelFace/ThirdAngelFace2_TabPort_dlqf9c.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523905/Blog/ThirdAngelFace/ThirdAngelFace3_TabLand_ilj35s.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523905/Blog/ThirdAngelFace/ThirdAngelFace4_Desk_lvntez.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523910/Blog/ThirdAngelFace/ThirdAngelFace5_DeskGr_as5tmb.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523911/Blog/ThirdAngelFace/ThirdAngelFace6_SmartR_jiktxa.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523909/Blog/ThirdAngelFace/ThirdAngelFace7_TabPorR_ecfguz.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523911/Blog/ThirdAngelFace/ThirdAngelFace8_TabLandR_wecnxt.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523905/Blog/ThirdAngelFace/ThirdAngelFace9_DeskR_xgv88j.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523908/Blog/ThirdAngelFace/ThirdAngelFace10_DeskGrR_xl1beq.jpg',
			altValue: 'Angel Face 3'
		}
	}
}

const AngelFaceComponent = () => (
	<div className='angelFaceWrapper'>
		<PhotographySection infoSection = {angelFaceInfo} />
	</div>
)

export default AngelFaceComponent;
