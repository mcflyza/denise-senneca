import React from 'react'
import PhotographySection from '../../photographySection/photographySection'

const recordingFlowersInfo = {
    titleSection: 'Recording Flowers',
    photographyInfo: {
        'Recording Flowers 1': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362833/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_1/01Smart_ylkxci.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362833/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_1/02TabPort_qvriho.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362831/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_1/03TabLand_tdb0lz.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362831/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_1/04Desk_jhlowx.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362834/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_1/05DeskGr_q93s2v.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362832/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_1/06SmartR_ijliaw.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362833/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_1/07TabPortR_cbpi24.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362831/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_1/08TabLandR_apppz2.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362831/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_1/09DeskR_pixzij.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362832/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_1/10DeskGr_dxe2aj.jpg',
            altValue: 'Recording Flowers 1',
        },
        'Recording Flowers 2': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362834/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_2/01Smart_smd7qo.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362835/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_2/02TabPort_qvh9xo.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362833/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_2/03TabLand_eps4jd.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362834/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_2/04Desk_cqjsiv.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362836/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_2/05DeskGr_yrsiiz.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362835/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_2/06SmartRjpg_d9pvjd.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362835/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_2/07TaBPortR_i02co1.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362834/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_2/08TabLandRjpg_a7chyj.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362834/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_2/09DeskRjpg_irxp9r.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362834/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_2/10DeskGRR_yhpajn.jpg',
            altValue: 'Recording Flowers 2'
        },
        'Recording Flowers 3': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362836/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_3/01Smart_hfvubl.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362836/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_3/02TabPort_vi7vme.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362835/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_3/03TabLand_n1biug.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362835/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_3/04Desk_bry7ch.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362837/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_3/05DeskGr_v0sgtj.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362836/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_3/06SmartR_u4i3vx.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362836/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_3/07TabPortR_xj1lqo.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362835/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_3/08TabLandR_julnna.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362836/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_3/09DeskR_b5z9lv.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362836/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_3/10DeskGrR_ltahjz.jpg',
            altValue: 'Recording Flowers 3',
        },
        'Recording Flowers 4': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362837/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_4/01Smart_dtbqtc.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362838/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_4/02TabPort_vc5xoz.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362837/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_4/03TabLand_ake4v9.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362837/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_4/04Desk_r3hhtj.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362838/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_4/05DeskGr_wdeyzx.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362837/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_4/06SmartR_zhr7vw.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362838/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_4/07TabPortR_jbweuv.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362837/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_4/08TabLandR_d04vmp.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362838/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_4/09DeskR_dxs9q5.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362838/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_4/10DeskGrR_flkoxw.jpg',
            altValue: 'Recording Flowers 4'
        },
        'Recording Flowers 5': {
            smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362832/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/01Smart_cko7is.jpg',
            tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362833/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/02TabPort_cedcxh.jpg',
            tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362838/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/03TabLand_suxb3p.jpg',
            desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362838/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/04Desk_fxphft.jpg',
            deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362833/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/05DeskGr_bu3u87.jpg',
            smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362831/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/06SmartR_s1b9i8.jpg',
            tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362832/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/07TabPortR_iwx8aa.jpg',
            tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362839/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/08TabLandR_sotocj.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362832/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/09DeskR_wqac7y.jpg',
            deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1570362832/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/10DeskGrR_wlcnle.jpg',
            altValue: 'Recording Flowers 5'
        }
    }
}

const RecordingFlowersComponent = () => (
    <div className='recordingFlowersWrapper'>
        <PhotographySection infoSection={recordingFlowersInfo} />
    </div>
)

export default RecordingFlowersComponent;
