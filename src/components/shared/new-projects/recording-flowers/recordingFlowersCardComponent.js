import React from 'react'
import PhotoCard from '../../photoCard/photoCard'

const recordingFlowersCardInfo = {
    link: '/new-projects/recording-flowers',
    title: 'Recording Flowers',
    srcSetImgs: {
        smart:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362832/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/01Smart_cko7is.jpg',
        tabPort:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362833/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/02TabPort_cedcxh.jpg',
        tabLand:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362838/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/03TabLand_suxb3p.jpg',
        desk:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362838/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/04Desk_fxphft.jpg',
        deskGr:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362833/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/05DeskGr_bu3u87.jpg',
        smartR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362831/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/06SmartR_s1b9i8.jpg',
        tabPortR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362832/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/07TabPortR_iwx8aa.jpg',
        tabLandR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362839/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/08TabLandR_sotocj.jpg',
        deskR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362832/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/09DeskR_wqac7y.jpg',
        deskGrR:
            'https://res.cloudinary.com/mcflyza/image/upload/v1570362832/NewProjects/RECORDING_FLOWERS/RECORDING_FLOWERS_5_THUMB/10DeskGrR_wlcnle.jpg',
        altValue: 'Recording Flowers',
    },
}

const RecordingFlowersCardComponent = () => <PhotoCard photoCardInfo={recordingFlowersCardInfo} />

export default RecordingFlowersCardComponent;
