import React from 'react'
import { Link } from 'gatsby'

import ResponsiveImage from '../responsiveImage/responsiveImage';

import { titleToClass } from '../../../static/utils/utils'

const PhotoCard = (props) => {

    let {
        link,
        title,
        srcSetImgs
    } = props.photoCardInfo

    let specificClass = titleToClass(title) + '_card_wrapper'

    let cardWrapperClassName = `photo_card_wrapper ${specificClass}`
    let sectionClassName = `photo_card_section ${specificClass + '__section'}`
    let titleClassName = `photo_card_section ${specificClass + '__section__title'}`
  
    return <>
                <div className = {cardWrapperClassName}>
                    <Link to = {link}>
                        <section className = {sectionClassName}>
                            <h3 className = {titleClassName}>{title}</h3>
                            <ResponsiveImage srcSetImgs = {srcSetImgs} />
                        </section>
                    </Link>
                </div>
            </>
}

export default PhotoCard;
