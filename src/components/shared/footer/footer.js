import React from "react"

import './footer.scss'

let year = new Date().getFullYear();

const Footer = () => (
    <footer className='mainFooter'>
        <p>{year} &copy; Denise Senneca</p>
    </footer>
)

export default Footer
