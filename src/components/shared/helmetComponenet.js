import React, { Component } from "react"
import { Helmet } from "react-helmet"

class HelmetComponent extends Component {
  render() {
    return (
      <div className="application">
        <Helmet>
          <meta name="google-site-verification" content="k0VLCPhyxb6l2L8zxfYkCsyT2RU26xqduAR2_phpyL8" />
          <meta charSet="utf-8" />
          <title>Denise Senneca</title>
          <link rel="canonical" href="http://denisesenneca.com" />
        </Helmet>
      </div>
    )
  }
}

export default HelmetComponent;