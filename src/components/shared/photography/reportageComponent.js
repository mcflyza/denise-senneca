import React from 'react'

import PhotographySection from '../photographySection/photographySection'

const reportageInfo = {
	titleSection: 'Reportage',
	photographyInfo: {
		'Mother Land': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523991/Photography/Reportage/1MotherLand/MotherLand1_Smart_bbffak.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523991/Photography/Reportage/1MotherLand/MotherLand2_TabPort_ummpo1.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523995/Photography/Reportage/1MotherLand/MotherLand3_TabLand_cugegz.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523995/Photography/Reportage/1MotherLand/MotherLand4_Desk_rpjqpf.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523991/Photography/Reportage/1MotherLand/MotherLand5_DeskGr_nn7bja.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523993/Photography/Reportage/1MotherLand/MotherLand6_SmartR_b4gw9n.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523995/Photography/Reportage/1MotherLand/MotherLand7_TabPortR_q8yxhw.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523992/Photography/Reportage/1MotherLand/MotherLand8_TabLandR_mhmorp.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523991/Photography/Reportage/1MotherLand/MotherLand9_DeskR_dhgknx.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523992/Photography/Reportage/1MotherLand/MotherLand10_DeskGrR_uvmp6v.jpg',
			altValue: 'Mother Land'
		},
		'The Man': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524008/Photography/Reportage/2TheMan/TheMan1_Smart_q2mtsq.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524007/Photography/Reportage/2TheMan/TheMan2_TabPort_rryjuv.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524003/Photography/Reportage/2TheMan/TheMan3_TabLand_r7tk8e.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524001/Photography/Reportage/2TheMan/TheMan4_Desk_xc0yeb.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524006/Photography/Reportage/2TheMan/TheMan5_DeskGr_hbe2uz.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524004/Photography/Reportage/2TheMan/TheMan6_SmartR_c7ihob.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524006/Photography/Reportage/2TheMan/TheMan7_TabPortR_swec0l.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524006/Photography/Reportage/2TheMan/TheMan8_TabLandR_myktek.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524006/Photography/Reportage/2TheMan/TheMan9_DeskR_xlcdck.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524006/Photography/Reportage/2TheMan/TheMan10_DeskGrR_tohjym.jpg',
			altValue: 'The Man'
		},
		'The Seller': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523995/Photography/Reportage/3TheSeller/TheSeller1_Smart_zigm9l.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524000/Photography/Reportage/3TheSeller/TheSeller2_TabPort_o2ygb2.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523998/Photography/Reportage/3TheSeller/TheSeller3_TabLand_mxzvld.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523998/Photography/Reportage/3TheSeller/TheSeller4_Desk_vdgaui.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524001/Photography/Reportage/3TheSeller/TheSeller5_DeskGr_pkl0sn.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523998/Photography/Reportage/3TheSeller/TheSeller6_SmartR_ckyz4m.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523996/Photography/Reportage/3TheSeller/TheSeller7_TabPortR_zwdkag.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523998/Photography/Reportage/3TheSeller/TheSeller8_TabLandR_xqukw2.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523998/Photography/Reportage/3TheSeller/TheSeller9_DeskR_h0xaz7.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566523998/Photography/Reportage/3TheSeller/TheSeller10_DeskGrR_tazwfs.jpg',
			altValue: 'The Seller'
		},
		'The Cross': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825871/Photography/Reportage/4TheCross/01Smart_zg12xx.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825871/Photography/Reportage/4TheCross/02Tab-Port_w5lwjh.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825871/Photography/Reportage/4TheCross/03Tab-Land_fqelrh.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825871/Photography/Reportage/4TheCross/04Desk_cvr5ky.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825871/Photography/Reportage/4TheCross/05Desk-Gr_q8dp4f.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825871/Photography/Reportage/4TheCross/06Smart-DisplayR_qepuc4.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825872/Photography/Reportage/4TheCross/07Tab-Port-DisplayR_zrnyqt.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825872/Photography/Reportage/4TheCross/08Tab-Land-DisplayR_rrb7nf.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825873/Photography/Reportage/4TheCross/09Desk-DisplayR_s97nri.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825873/Photography/Reportage/4TheCross/10Desk-Gr-DisplayR_xgvucv.jpg',
			altValue: 'The Cross'
		},
		'I Was Born Already Old': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826134/Photography/Reportage/5IWasBornAlreadyOld/01Smart_twsffa.jpg	',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826135/Photography/Reportage/5IWasBornAlreadyOld/02Tab-Port_hxpnis.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826135/Photography/Reportage/5IWasBornAlreadyOld/03Tab-Land_guntvc.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826135/Photography/Reportage/5IWasBornAlreadyOld/04Desk_u3jgfu.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826135/Photography/Reportage/5IWasBornAlreadyOld/05Desk-Gr_sdphhi.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826135/Photography/Reportage/5IWasBornAlreadyOld/06Smart-DisplayR_zdglde.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826137/Photography/Reportage/5IWasBornAlreadyOld/07Tab-Port-DisplayR_hgefkq.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826136/Photography/Reportage/5IWasBornAlreadyOld/08Tab-Land-DisplayR_eeonmm.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826137/Photography/Reportage/5IWasBornAlreadyOld/09Desk-DisplayR_pxdfoi.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826137/Photography/Reportage/5IWasBornAlreadyOld/10Desk-Gr-DisplayR_ln66nb.jpg',
			altValue: 'I Was Born Already Old'
		}
	}
}

const reportageComponent = () => (
  <>
    <div className = 'reportageWrapper'>
		<PhotographySection infoSection = {reportageInfo}/>
	</div>
  </>
)

export default reportageComponent;
