import React from 'react'

import PhotographySection from '../photographySection/photographySection'

const streetInfo = {
	titleSection: 'Street',
	photographyInfo: {
		'Life Is Strange': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524019/Photography/Street/1LifeIsStrange/LIF1_Smart_qp1kgt.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524019/Photography/Street/1LifeIsStrange/LIF2_TabPort_teaqvq.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524022/Photography/Street/1LifeIsStrange/LIF3_TabLand_jbzswz.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524022/Photography/Street/1LifeIsStrange/LIF4_Desk_oy1ish.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524019/Photography/Street/1LifeIsStrange/LIF5_DeskGr_fqrvek.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524020/Photography/Street/1LifeIsStrange/LIF6_SmartR_htjleo.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524022/Photography/Street/1LifeIsStrange/LIF7_TabPortR_izwfag.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524022/Photography/Street/1LifeIsStrange/LIF8_TabLandR_vbpqrn.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524019/Photography/Street/1LifeIsStrange/LIF9_DeskR_nhcnvo.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524023/Photography/Street/1LifeIsStrange/LIF10_DeskGrR_swuyuh.jpg',
			altValue: 'Life Is Strange'
		},
		'Wait': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524028/Photography/Street/2Wait/Wait1_Smart_mmwnvp.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524028/Photography/Street/2Wait/Wait2_TabPort_uoukmt.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524025/Photography/Street/2Wait/Wait3_TabLand_kj5nbc.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524025/Photography/Street/2Wait/Wait4_Desk_tvrfrk.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524025/Photography/Street/2Wait/Wait5_DeskGr_pdhgij.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524025/Photography/Street/2Wait/Wait6_SmartR_xuitkt.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524024/Photography/Street/2Wait/Wait7_TabPortR_xg0cbi.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524025/Photography/Street/2Wait/Wait8_TabLandR_d497hq.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524025/Photography/Street/2Wait/Wait9_DeskR_brxgly.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524028/Photography/Street/2Wait/Wait10_DeskGrR_m9kpyy.jpg',
			altValue: 'Wait'
		},
		'Fire': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825532/Photography/Street/5Fire/01Smart_gilefo.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825532/Photography/Street/5Fire/02Tab-Port_rgopop.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825532/Photography/Street/5Fire/03Tab-Land_vr8mrh.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825532/Photography/Street/5Fire/04Desk_sfhozh.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825533/Photography/Street/5Fire/05Desk-Gr_n0wf14.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825533/Photography/Street/5Fire/06Smart-DisplayR_ome0va.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825532/Photography/Street/5Fire/07Tab-Port-DisplayR_junarl.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825532/Photography/Street/5Fire/08Tab-Land-DisplayR_yprfcp.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825533/Photography/Street/5Fire/09Desk-DisplayR_foagsh.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587825533/Photography/Street/5Fire/10Desk-Gr-DisplayR_ejexny.jpg',
			altValue: 'Fire'
		},
		'The Sky Above Napoli': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524010/Photography/Street/3TheSkyAboveNapoli/TheSkyAboveNapoli1_Smart_oj3ccn.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524010/Photography/Street/3TheSkyAboveNapoli/TheSkyAboveNapoli2_TabPort_rypu37.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524014/Photography/Street/3TheSkyAboveNapoli/TheSkyAboveNapoli3_TabLand_xhtj7y.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524010/Photography/Street/3TheSkyAboveNapoli/TheSkyAboveNapoli4_Desk_dxyufd.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524012/Photography/Street/3TheSkyAboveNapoli/TheSkyAboveNapoli5_DeskGr_ljyoyq.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524012/Photography/Street/3TheSkyAboveNapoli/TheSkyAboveNapoli6_SmartR_btegwk.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524012/Photography/Street/3TheSkyAboveNapoli/TheSkyAboveNapoli7_TabPortR_urbwng.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524012/Photography/Street/3TheSkyAboveNapoli/TheSkyAboveNapoli8_TabLandR_c9nqdo.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524010/Photography/Street/3TheSkyAboveNapoli/TheSkyAboveNapoli9_DeskR_s6ct2g.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524010/Photography/Street/3TheSkyAboveNapoli/TheSkyAboveNapoli10_DeskGrR_gv7uba.jpg',
			altValue: 'The Sky Above Napoli'
		},
		'The Sky Above Barcelona': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826430/Photography/Street/6TheSkyAboveBarcelona/01Smart_qxvp6g.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826430/Photography/Street/6TheSkyAboveBarcelona/02Tab-Port_cbsogm.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826430/Photography/Street/6TheSkyAboveBarcelona/03Tab-Land_xoygyf.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826430/Photography/Street/6TheSkyAboveBarcelona/04Desk_qkw6f5.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826430/Photography/Street/6TheSkyAboveBarcelona/05Desk-Gr_tgrxj9.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826430/Photography/Street/6TheSkyAboveBarcelona/06Smart-DisplayR_jwtftg.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826431/Photography/Street/6TheSkyAboveBarcelona/07Smart-Port-DisplayR_pgpw80.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826431/Photography/Street/6TheSkyAboveBarcelona/08Smart-Land-DisplayR_hfqiu3.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826431/Photography/Street/6TheSkyAboveBarcelona/09Desk-DisplayR_lpqsx2.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1587826432/Photography/Street/6TheSkyAboveBarcelona/10Desk-Gr-DisplayR_l1ejtl.jpg',
			altValue: 'The Sky Above Barcelona'
		}/*,
		'The Grandma': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524014/Photography/Street/4TheGrandma/TheGrandma1_Smart_lypjuj.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524014/Photography/Street/4TheGrandma/TheGrandma2_TabPort_herloz.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524017/Photography/Street/4TheGrandma/TheGrandma3_TabLand_plrinq.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524016/Photography/Street/4TheGrandma/TheGrandma4_Desk_j6tipj.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524014/Photography/Street/4TheGrandma/TheGrandma5_DeskGr_hbhtvr.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524016/Photography/Street/4TheGrandma/TheGrandma6_SmartR_i55fmq.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524017/Photography/Street/4TheGrandma/TheGrandma7_TabPortR_nmp5xa.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524017/Photography/Street/4TheGrandma/TheGrandma8_TabLandR_jnxbim.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524016/Photography/Street/4TheGrandma/TheGrandma9_DeskR_ll1xs3.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524016/Photography/Street/4TheGrandma/TheGrandma10_DeskGrR_vxlh5s.jpg',
			altValue: 'The Grandma'
		}*/
	}
}

const StreetComponent = () => (
  <>
    <div className = 'streetWrapper'>
		<PhotographySection infoSection = {streetInfo}/>
	</div>
  </>
)

export default StreetComponent;
