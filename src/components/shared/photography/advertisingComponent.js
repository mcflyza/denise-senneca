import React from 'react'

import PhotographySection from '../photographySection/photographySection'

const advertisingInfo = {
	titleSection: 'Advertising',
	photographyInfo: {
		'I Want To Be Something Else': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524043/Photography/Advertising/1IWantToBeSomethingElse/IWantToBeSomethingElse1_Smart_tp0ccc.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524045/Photography/Advertising/1IWantToBeSomethingElse/IWantToBeSomethingElse2_TabPort_zppf04.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524046/Photography/Advertising/1IWantToBeSomethingElse/IWantToBeSomethingElse3_TabLand_t3ytg0.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524046/Photography/Advertising/1IWantToBeSomethingElse/IWantToBeSomethingElse4_Desk_yn4wqg.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524043/Photography/Advertising/1IWantToBeSomethingElse/IWantToBeSomethingElse5_DeskGr_twqduo.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524046/Photography/Advertising/1IWantToBeSomethingElse/IWantToBeSomethingElse6_SmartR_beqdsu.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524043/Photography/Advertising/1IWantToBeSomethingElse/IWantToBeSomethingElse7_TabPortR_aqhjqe.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524043/Photography/Advertising/1IWantToBeSomethingElse/IWantToBeSomethingElse8_TabLandR_luqlvk.jpg',
            deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524043/Photography/Advertising/1IWantToBeSomethingElse/IWantToBeSomethingElse9_DeskR_n468su.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524046/Photography/Advertising/1IWantToBeSomethingElse/IWantToBeSomethingElse10_DeskGrR_mzihli.jpg',
			altValue: 'I Want To Be Something Else'
		},
		'Excited Milk': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524032/Photography/Advertising/2ExcitedMilk/ExcitedMilk1_Smart_b1ungl.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524032/Photography/Advertising/2ExcitedMilk/ExcitedMilk2_TabPort_ir2agc.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524030/Photography/Advertising/2ExcitedMilk/ExcitedMilk3_TabLand_ua79fq.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524032/Photography/Advertising/2ExcitedMilk/ExcitedMilk4_Desk_g4suwl.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524032/Photography/Advertising/2ExcitedMilk/ExcitedMilk5_DeskGr_w5fqbr.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524028/Photography/Advertising/2ExcitedMilk/ExcitedMilk6_SmartR_bcvbdy.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524034/Photography/Advertising/2ExcitedMilk/ExcitedMilk7_TabPortR_j4d9vy.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524032/Photography/Advertising/2ExcitedMilk/ExcitedMilk8_TabLandR_daewpk.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524028/Photography/Advertising/2ExcitedMilk/ExcitedMilk9_DeskR_ylsj3y.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524032/Photography/Advertising/2ExcitedMilk/ExcitedMilk10_DeskGrR_n4r1lj.jpg',
			altValue: 'Excited Milk'
		},
		'Beauty': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524050/Photography/Advertising/3Beauty/Beauty1_Smart_zdya6e.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524047/Photography/Advertising/3Beauty/Beauty2_TabPort_bct9pz.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524050/Photography/Advertising/3Beauty/Beauty3_TabLand_m0epuh.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524050/Photography/Advertising/3Beauty/Beauty4_Desk_uvwgix.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524050/Photography/Advertising/3Beauty/Beauty5_DeskGr_ol83ok.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524050/Photography/Advertising/3Beauty/Beauty6_SmartR_nxqcqx.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524051/Photography/Advertising/3Beauty/Beauty7_TabPortR_quhihm.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524047/Photography/Advertising/3Beauty/Beauty8_TabLandR_qc3t2e.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524050/Photography/Advertising/3Beauty/Beauty9_DeskR_nqiaou.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524046/Photography/Advertising/3Beauty/Beauty10_DeskGrR_rectyf.jpg',
			altValue: 'Beauty'
		},
		'Metals': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524036/Photography/Advertising/4Metals/Metals1_Smart_p4pxyq.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524034/Photography/Advertising/4Metals/Metals2_TabPort_bey6yf.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524039/Photography/Advertising/4Metals/Metals3_TabLand_rysv40.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524040/Photography/Advertising/4Metals/Metals4_Desk_grqtim.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524039/Photography/Advertising/4Metals/Metals5_DeskGr_ptnrmr.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524042/Photography/Advertising/4Metals/Metals6_SmartR_vdktwf.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524042/Photography/Advertising/4Metals/Metals7_TabPortR_l82d9a.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524042/Photography/Advertising/4Metals/Metals8_TabLandR_zrsxad.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524039/Photography/Advertising/4Metals/Metals9_DeskR_ga09gr.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524038/Photography/Advertising/4Metals/Metals10_DeskGrR_jpxyar.jpg',
			altValue: 'Metals'
		}
	}
}

const AdvertisingComponent = () => (
	<>
		<div className = 'advertisingWrapper'>
			<PhotographySection infoSection = {advertisingInfo}/>
		</div>
	</>
)

export default AdvertisingComponent;
