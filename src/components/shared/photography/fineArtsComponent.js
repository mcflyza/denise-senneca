import React from 'react'

import PhotographySection from '../photographySection/photographySection'

const fineArtsInfo = {
	titleSection: 'Fine Arts',
	photographyInfo: {
		'Live In Plastic': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524052/Photography/FineArts/1LiveInPlastic/LiveInPlastic1_Smart_nwcupa.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524055/Photography/FineArts/1LiveInPlastic/LiveInPlastic2_TabPort_ssc2dq.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524052/Photography/FineArts/1LiveInPlastic/LiveInPlastic3_TabLand_oailmp.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524055/Photography/FineArts/1LiveInPlastic/LiveInPlastic4_Desk_vdkp8t.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524052/Photography/FineArts/1LiveInPlastic/LiveInPlastic5_DeskGr_igrccv.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524051/Photography/FineArts/1LiveInPlastic/LiveInPlastic6_SmartR_sqanjn.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524055/Photography/FineArts/1LiveInPlastic/LiveInPlastic7_TabPortR_jhnj2s.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524055/Photography/FineArts/1LiveInPlastic/LiveInPlastic8_TabLandR_ndkpdr.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524055/Photography/FineArts/1LiveInPlastic/LiveInPlastic9_DeskR_f6udse.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524056/Photography/FineArts/1LiveInPlastic/LiveInPlastic10_DeskGrR_idh10l.jpg',
			altValue: 'Live In Plastic'
		},
		'Meet Your Meat': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524056/Photography/FineArts/2MeetYourMeat/MeetyourMeat1_Smart_hjl7an.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524057/Photography/FineArts/2MeetYourMeat/MeetyourMeat2_TabPort_aqxe2k.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524056/Photography/FineArts/2MeetYourMeat/MeetyourMeat3_TabLand_szy5vl.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524057/Photography/FineArts/2MeetYourMeat/MeetyourMeat4_Desk_t2bjlw.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524057/Photography/FineArts/2MeetYourMeat/MeetyourMeat5_DeskGr_p5am7m.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524056/Photography/FineArts/2MeetYourMeat/MeetyourMeat6_SmartR_v7huug.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524056/Photography/FineArts/2MeetYourMeat/MeetYourMeat7_TabPortR_cskyt8.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524056/Photography/FineArts/2MeetYourMeat/MeetYourMeat8_TabLandR_hkhgem.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524056/Photography/FineArts/2MeetYourMeat/MeetYourMeat9_DeskR_gjtdus.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524057/Photography/FineArts/2MeetYourMeat/MeetYourMeat10_DeskGrR_rbqogt.jpg',
			altValue: 'Meet Your Meat'
		},
		'Touch': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524057/Photography/FineArts/3Touch/Touch1_Smart_yf2ac6.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524059/Photography/FineArts/3Touch/Touch2_TabPort_j26ln2.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524057/Photography/FineArts/3Touch/Touch3_TabLand_ktuwj3.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524057/Photography/FineArts/3Touch/Touch4_Desk_bf5ta8.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524059/Photography/FineArts/3Touch/Touch7_TabPortR_lfpaju.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524057/Photography/FineArts/3Touch/Touch6_SmartR_mf9c9k.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524059/Photography/FineArts/3Touch/Touch7_TabPortR_lfpaju.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524058/Photography/FineArts/3Touch/Touch8_TabLandR_qitce3.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524057/Photography/FineArts/3Touch/Touch9_DeskR_ccftep.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524058/Photography/FineArts/3Touch/Touch10_DeskGrR_vrbmol.jpg',
			altValue: 'Touch'
		},
		'Behind Me': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524059/Photography/FineArts/4BehindMe/BehindMe1_Smart_xyuoop.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524060/Photography/FineArts/4BehindMe/BehindMe2_TabPort_liqj5l.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524059/Photography/FineArts/4BehindMe/BehindMe3_TabLand_yhrcvi.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524059/Photography/FineArts/4BehindMe/BehindMe4_Desk_kajf0p.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524060/Photography/FineArts/4BehindMe/BehindMe5_DeskGr_spltsl.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524059/Photography/FineArts/4BehindMe/BehindMe6_SmartR_axe4co.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524060/Photography/FineArts/4BehindMe/BehindMe7_TabPortR_xl8mjb.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524060/Photography/FineArts/4BehindMe/BehindMe8_TabLandR_nnptbp.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524059/Photography/FineArts/4BehindMe/BehindMe9_DeskR_kp9oyh.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524059/Photography/FineArts/4BehindMe/BehindMe10_DeskGrR_aydlfs.jpg',
			altValue: 'Behind Me'
		},
		'Red Light': {
			smart: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524051/Photography/FineArts/5RedLight/RedLight1_Smart_azb43r.jpg',
			tabPort: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524051/Photography/FineArts/5RedLight/RedLight2_TabPort_bgr7rk.jpg',
			tabLand: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524051/Photography/FineArts/5RedLight/RedLight3_TabLand_nsgjyv.jpg',
			desk: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524051/Photography/FineArts/5RedLight/RedLight4_Desk_vtxyqb.jpg',
			deskGr: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524051/Photography/FineArts/5RedLight/RedLight5_DeskGr_yrnh6l.jpg',
			smartR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524051/Photography/FineArts/5RedLight/RedLight6_SmartR_jjnygj.jpg',
			tabPortR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524051/Photography/FineArts/5RedLight/RedLight7_TabPorR_cku74r.jpg',
			tabLandR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524051/Photography/FineArts/5RedLight/RedLight8_TabLandR_hn43kb.jpg',
			deskR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524051/Photography/FineArts/5RedLight/RedLight9_DeskR_emel1n.jpg',
			deskGrR: 'https://res.cloudinary.com/mcflyza/image/upload/v1566524051/Photography/FineArts/5RedLight/RedLight10_DeskGrR_iuieoz.jpg',
			altValue: 'Red Light'
		}
	}
}

const fineArtsComponent = () => (
  <>
    <div className = 'fineArtsWrapper'>
		<PhotographySection infoSection = {fineArtsInfo}/>
	</div>
  </>
)

export default fineArtsComponent;
