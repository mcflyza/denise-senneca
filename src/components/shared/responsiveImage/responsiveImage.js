import React from 'react'

const ResponsiveImage = (props) => {

	let {
		smart,
		tabPort,
		tabLand,
		desk,
		deskGr,
		smartR,
		tabPortR,
		tabLandR,
		deskR,
		deskGrR,
		altValue
	} = props.srcSetImgs;

	return (
		<>
			<picture>
				<source 
					srcSet = {`
						${smart} 600w, 
						${tabPort} 916w, 
						${tabLand} 1080w,
						${desk} 1280w,
						${deskGr} 2160w
					`}
				/>
				<source 
					srcSet = {`
						${smartR} 2x, 
						${tabPortR} 2x, 
						${tabLandR} 2x,
						${deskR} 2x,
						${deskGrR} 2x
					`}
				/>
				<img className = {props.className && props.className}
					src = {smart}
					alt = {altValue}
				/>
			</picture>
		</>
	);
	
}

export default ResponsiveImage;
